package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.auth.AuthRepository
import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.LoginResponseData
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponseData
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.AuthViewModel
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AuthViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var authRepository: AuthRepository

    @Mock
    lateinit var analyticsManager: AnalyticsManager

    @Mock
    lateinit var registerObserver: Observer<Resource<RegisterResponse>>

    @Mock
    lateinit var loginObserver: Observer<Resource<LoginResponse>>

    private lateinit var authViewModel: AuthViewModel

    @Before
    fun setUp() {
        authViewModel = AuthViewModel(authRepository, analyticsManager)
        authViewModel.registerState.observeForever(registerObserver)
        authViewModel.loginState.observeForever(loginObserver)
    }

    @After
    fun tearDown() {
        authViewModel.registerState.removeObserver(registerObserver)
        authViewModel.loginState.removeObserver(loginObserver)
    }

    @Test
    fun signUpUser() = runTest {
        val response = RegisterResponse(
            code = 200,
            message = "OK",
            data = RegisterResponseData("", 0, "")
        )

        Mockito.`when`(authRepository.registerAccount("", "")).thenReturn(
            flowOf(Resource.Success(response))
        )

        authViewModel.signUpAccount("", "")
        Mockito.verify(authRepository).registerAccount("", "")
    }

    @Test
    fun signInUser() = runTest {
        val response = LoginResponse(
            code = 200,
            message = "OK",
            data = LoginResponseData("", "", "", 0, "")
        )

        Mockito.`when`(authRepository.loginAccount("", "")).thenReturn(
            flowOf(Resource.Success(response))
        )

        authViewModel.signInAccount("","")
        Mockito.verify(authRepository).loginAccount("","")
    }
}