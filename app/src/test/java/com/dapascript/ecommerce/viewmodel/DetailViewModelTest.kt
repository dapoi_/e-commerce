package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.data.repository.detail.DetailRepository
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.DetailViewModel
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var detailRepository: DetailRepository

    @Mock
    lateinit var cartRepository: CartRepository

    @Mock
    lateinit var analyticsManager: AnalyticsManager

    private lateinit var detailViewModel: DetailViewModel

    @Before
    fun setup() {
        detailViewModel = DetailViewModel(detailRepository, cartRepository, analyticsManager)
    }

    @Test
    fun getDetail() = runTest {
        val response = DetailItem(
            emptyList(), "", "", 0, "", "", 0, 0,
            emptyList(), 0, Any(), "", 0, 0
        )
        Mockito.`when`(detailRepository.getProduct("")).thenReturn(
            flowOf(Resource.Success(response))
        )

        detailViewModel.getDetail("")
    }
}
