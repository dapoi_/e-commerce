package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dapascript.ecommerce.data.repository.profile.ProfileRepository
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponseData
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.ProfileViewModel
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class ProfileViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var profileRepository: ProfileRepository

    private lateinit var profileViewModel: ProfileViewModel

    @Before
    fun setUp() {
        profileViewModel = ProfileViewModel(profileRepository)
    }

    @Test
    fun createProfile() = runTest {
        val userName = "".toRequestBody("text/plain".toMediaTypeOrNull())
        val response = ProfileResponse(
            code = 200,
            message = "OK",
            data = ProfileResponseData("", "")
        )

        `when`(profileRepository.createProfile(userName, null)).thenReturn(
            flowOf(Resource.Success(response))
        )

        profileViewModel.createProfile(userName, null)
    }
}