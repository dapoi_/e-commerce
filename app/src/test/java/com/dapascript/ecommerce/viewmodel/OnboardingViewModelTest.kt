package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dapascript.ecommerce.data.preference.DataStorePreference
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class OnboardingViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var dataStorePreference: DataStorePreference

    @Mock
    private lateinit var dataStoreViewModel: DataStoreViewModel

    @Before
    fun setUp() {
        dataStoreViewModel = DataStoreViewModel(dataStorePreference)
    }

    @Test
    fun saveOnboardingTest() = runTest {
        val state = true
        dataStoreViewModel.saveOnboardingState(true)

        verify(dataStorePreference).saveOnboardingState(state)
    }
}