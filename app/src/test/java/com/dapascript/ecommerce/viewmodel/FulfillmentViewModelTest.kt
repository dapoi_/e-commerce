package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dapascript.ecommerce.data.repository.cart.fulfillment.FulfillmentRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponseItem
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.FulfillmentViewModel
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class FulfillmentViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var fulfillmentRepositoryImpl: FulfillmentRepositoryImpl

    @Mock
    lateinit var fulfillmentObserver: Observer<Resource<FulfillmentResponse>>

    @Mock
    lateinit var rateObserver: Observer<Resource<RatingResponse>>

    private lateinit var fulfillmentViewModel: FulfillmentViewModel

    @Before
    fun setUp() {
        fulfillmentViewModel = FulfillmentViewModel(fulfillmentRepositoryImpl)
        fulfillmentViewModel.postFulfillment.observeForever(fulfillmentObserver)
        fulfillmentViewModel.ratingProduct.observeForever(rateObserver)
    }

    @After
    fun close() {
        fulfillmentViewModel.postFulfillment.removeObserver(fulfillmentObserver)
        fulfillmentViewModel.ratingProduct.removeObserver(rateObserver)
    }

    @Test
    fun getFulfillment() = runTest {
        val response = FulfillmentResponse(
            code = 200,
            message = "",
            data = FulfillmentResponseItem("", 0, "", "", "", true)
        )
        Mockito.`when`(fulfillmentRepositoryImpl.postFullfillment("", "", "", 0)).thenReturn(
            flowOf(Resource.Success(response))
        )

        fulfillmentViewModel.postFullfillment("", "", "", 0)
    }

    @Test
    fun rateProduct() = runTest {
        val response = RatingResponse(
            code = 200,
            message = ""
        )
        Mockito.`when`(fulfillmentRepositoryImpl.rateProduct("", null, null))
            .thenReturn(flowOf(Resource.Success(response)))

        fulfillmentViewModel.rateProduct("", null, null)
    }
}