package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.CartViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class CartViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var cartRepository: CartRepository

    private lateinit var cartViewModel: CartViewModel

    @Before
    fun setUp() {
        cartViewModel = CartViewModel(cartRepository)
    }

    @Test
    fun getCart() = runTest {
        Mockito.`when`(cartRepository.getCartProduct()).thenReturn(flowOf(emptyList()))

        cartViewModel.getCartProduct()
    }
}