package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dapascript.ecommerce.data.repository.main.transaction.TransactionRepository
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.presentation.viewmodel.TransactionViewModel
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TransactionViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    lateinit var transactionRepository: TransactionRepository

    private lateinit var transactionViewModel: TransactionViewModel

    @Before
    fun setUp() {
        transactionViewModel = TransactionViewModel(transactionRepository)
    }

    @Test
    fun getTransaction() = runTest {
        val response = emptyList<TransactionHistoryData>()
        Mockito.`when`(transactionRepository.getTransactionRepository()).thenReturn(
            flowOf(Resource.Success(response))
        )

        transactionViewModel.getTransactionResult().observeForever {
            Assert.assertEquals(Resource.Success(response), it)
        }
    }
}