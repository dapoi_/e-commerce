package com.dapascript.ecommerce.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.main.store.StoreRepository
import com.dapascript.ecommerce.presentation.viewmodel.StoreViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.MockitoAnnotations
import kotlin.coroutines.ContinuationInterceptor

@ExperimentalCoroutinesApi
class StoreViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = CoroutineTestRule()

    @Mock
    lateinit var storeRepository: StoreRepository

    @Mock
    lateinit var analyticsManager: AnalyticsManager

    @Mock
    lateinit var filterProductObserver: Observer<StoreViewModel.FilterProduct>

    @Mock
    lateinit var isLinearLayoutObserver: Observer<Boolean>

    private lateinit var viewModel: StoreViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        viewModel = StoreViewModel(storeRepository, analyticsManager)
        viewModel.filterProduct.observeForever(filterProductObserver)
        viewModel.isLinearLayout.observeForever(isLinearLayoutObserver)
    }

    @After
    fun tearDown() {
        viewModel.filterProduct.removeObserver(filterProductObserver)
        viewModel.isLinearLayout.removeObserver(isLinearLayoutObserver)
    }

    @Test
    fun setListProduct_sameFilter_noUpdate() {
        val filterProduct = StoreViewModel.FilterProduct(chipSort = "Price")

        viewModel.setListProduct(filterProduct)
        viewModel.setListProduct(filterProduct)

        verifyNoMoreInteractions(storeRepository)
    }

    @Test
    fun setStateChip() {
        val filterProduct = StoreViewModel.FilterProduct(chipSort = "Price")

        viewModel.setStateChip(filterProduct)

        verify(analyticsManager).selectItemFilterEvent(filterProduct)
    }

    @ExperimentalCoroutinesApi
    class CoroutineTestRule : TestWatcher(), TestCoroutineScope by TestCoroutineScope() {
        override fun starting(description: Description?) {
            super.starting(description)
            Dispatchers.setMain(this.coroutineContext[ContinuationInterceptor] as CoroutineDispatcher)
        }

        override fun finished(description: Description?) {
            super.finished(description)
            Dispatchers.resetMain()
            this.cleanupTestCoroutines()
        }
    }
}