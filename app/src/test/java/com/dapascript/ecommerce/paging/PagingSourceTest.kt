package com.dapascript.ecommerce.paging

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.data.source.remote.PagingSource
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.helper.Dummy
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class PagingSourceTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var storePagingSource: PagingSource

    @Before
    fun setUp() {
        storePagingSource = PagingSource(apiService, null, null, null, null, null)
    }

    @Test
    fun getPagingSource() = runTest {
        val dummy = Dummy.getProductSuccess
        Mockito.`when`(
            apiService.getProduct(
                null,
                null,
                null,
                null,
                null,
                10,
                1
            )
        ).thenReturn(dummy)

        val productEntity = dummy.data.items.map {
                ProductEntity(
                    it.productId,
                    it.image,
                    it.productName,
                    it.productPrice,
                    it.store,
                    it.productRating.toString(),
                    it.sale.toString(),
                    it.brand
                )
        }

        assertEquals(
            androidx.paging.PagingSource.LoadResult.Page(
                data = productEntity,
                prevKey = null,
                nextKey = null
            ),
            storePagingSource.load(
                androidx.paging.PagingSource.LoadParams.Refresh(
                    key = null,
                    loadSize = 10,
                    placeholdersEnabled = false
                )
            )
        )
    }
}