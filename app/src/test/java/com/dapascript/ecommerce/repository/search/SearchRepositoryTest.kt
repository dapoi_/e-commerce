package com.dapascript.ecommerce.repository.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.main.store.search.SearchRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.model.SearchResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class SearchRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var searchRepositoryImpl: SearchRepositoryImpl

    @Before
    fun setUp() {
        searchRepositoryImpl = SearchRepositoryImpl(apiService, coroutinesTestRule.testDispatcher)
    }

    @Test
    fun searchProduct() = runTest {
        val dummy = SearchResponse(200, "OK", listOf("", ""))

        Mockito.`when`(apiService.searchProduct("Lenovo")).thenReturn(dummy)

        searchRepositoryImpl.searchProduct("Lenovo").drop(1).first().let {
            Assert.assertEquals(Resource.Success(dummy.data), it)
        }
    }
}