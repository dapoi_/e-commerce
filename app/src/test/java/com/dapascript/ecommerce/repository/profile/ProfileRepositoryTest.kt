package com.dapascript.ecommerce.repository.profile

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.profile.ProfileRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.helper.Dummy
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class ProfileRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var profileRepositoryImpl: ProfileRepositoryImpl

    @Before
    fun setUp() {
        profileRepositoryImpl = ProfileRepositoryImpl(apiService, coroutinesTestRule.testDispatcher)
    }

    @Test
    fun createProfileLoading() = runTest {
        val dataDummy = Dummy.dummyProfile

        `when`(apiService.createProfile("".toRequestBody(), null)).thenReturn(dataDummy)

        profileRepositoryImpl.createProfile("".toRequestBody(), null).drop(0).first().let {
            Assert.assertEquals(Resource.Loading, it)
        }
    }

    @Test
    fun createProfileSuccess() = runTest {
        val dataDummy = Dummy.dummyProfile
        val userName = "".toRequestBody("text/plain".toMediaTypeOrNull())

        `when`(apiService.createProfile(userName, null)).thenReturn(dataDummy)

        profileRepositoryImpl.createProfile(userName, null).drop(1).first().let {
            Assert.assertEquals(Resource.Success(dataDummy), it)
        }
    }

    @Test
    fun createProfileError() = runTest {
        val runtimeException = RuntimeException()
        val userName = "".toRequestBody("text/plain".toMediaTypeOrNull())

        `when`(apiService.createProfile(userName, null)).thenThrow(runtimeException)

        profileRepositoryImpl.createProfile(userName, null).drop(1).first().let {
            Assert.assertEquals(Resource.Error(runtimeException), it)
        }
    }
}