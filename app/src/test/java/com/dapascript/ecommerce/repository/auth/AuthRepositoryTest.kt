package com.dapascript.ecommerce.repository.auth

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.auth.AuthRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.model.AuthRequest
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.helper.Dummy
import com.dapascript.ecommerce.vo.Resource
import com.google.android.gms.tasks.Tasks
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class AuthRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    @Mock
    private lateinit var firebaseMessaging: FirebaseMessaging

    private lateinit var authRepositoryImpl: AuthRepositoryImpl

    @Before
    fun setUp() {
        `when`(firebaseMessaging.token).thenReturn(Tasks.forResult(""))
        authRepositoryImpl =
            AuthRepositoryImpl(apiService, firebaseMessaging, coroutinesTestRule.testDispatcher)
    }

    @Test
    fun signUpLoading() = runTest {
        val dataDummy = Dummy.dummyRegister
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.registerAccount(authRequest)).thenReturn(dataDummy)

        authRepositoryImpl.registerAccount("", "").drop(0).first().let {
            Assert.assertEquals(Resource.Loading, it)
        }
    }

    @Test
    fun signUpSuccess() = runTest {
        val dataDummy = Dummy.dummyRegister
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.registerAccount(authRequest)).thenReturn(dataDummy)

        authRepositoryImpl.registerAccount("", "").drop(1).first().let {
            Assert.assertEquals(Resource.Success(dataDummy), it)
        }
    }

    @Test
    fun signUpError() = runTest {
        val runtimeException = RuntimeException()
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.registerAccount(authRequest)).thenThrow(runtimeException)

        authRepositoryImpl.registerAccount("", "").drop(1).first().let {
            Assert.assertEquals(Resource.Error(runtimeException), it)
        }
    }

    @Test
    fun signInLoading() = runTest {
        val dataDummy = Dummy.dummyLogin
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.loginAccount(authRequest)).thenReturn(dataDummy)

        authRepositoryImpl.loginAccount("", "").drop(0).first().let {
            Assert.assertEquals(Resource.Loading, it)
        }
    }

    @Test
    fun sigInSuccess() = runTest {
        val dataDummy = Dummy.dummyLogin
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.loginAccount(authRequest)).thenReturn(dataDummy)

        authRepositoryImpl.loginAccount("", "").drop(1).first().let {
            Assert.assertEquals(Resource.Success(dataDummy), it)
        }
    }

    @Test
    fun signInError() = runTest {
        val runtimeException = RuntimeException()
        val authRequest = AuthRequest("", "", "")

        `when`(apiService.loginAccount(authRequest)).thenThrow(runtimeException)

        authRepositoryImpl.loginAccount("", "").drop(1).first().let {
            Assert.assertEquals(Resource.Error(runtimeException), it)
        }
    }
}