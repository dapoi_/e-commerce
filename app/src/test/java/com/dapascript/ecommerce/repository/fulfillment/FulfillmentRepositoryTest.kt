package com.dapascript.ecommerce.repository.fulfillment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.cart.fulfillment.FulfillmentRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentItem
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentRequest
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponseItem
import com.dapascript.ecommerce.data.source.remote.model.RatingRequest
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class FulfillmentRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var fulfillmentRepositoryImpl: FulfillmentRepositoryImpl

    @Before
    fun setUp() {
        fulfillmentRepositoryImpl =
            FulfillmentRepositoryImpl(apiService, coroutinesTestRule.testDispatcher)
    }

    @Test
    fun getFulfillment() = runTest {
        val dummy = FulfillmentResponse(
            200, message = "OK", data = FulfillmentResponseItem(
                "", 0, "", "", "", true
            )
        )
        val fulfillmentItem = FulfillmentItem("", "", 0)

        Mockito.`when`(
            apiService.postFulfillment(
                FulfillmentRequest(
                    "", listOf(fulfillmentItem)
                )
            )
        ).thenReturn(dummy)

        fulfillmentRepositoryImpl.postFullfillment("", "", "", 0).drop(1).first().let {
            Assert.assertEquals(Resource.Success(dummy), it)
        }
    }

    @Test
    fun rateProduct() = runTest {
        val dummy = RatingResponse(200, "OK")
        Mockito.`when`(
            apiService.ratingProduct(
                RatingRequest("", null, null)
            )
        ).thenReturn(dummy)

        fulfillmentRepositoryImpl.rateProduct("", null, null).drop(1).first().let {
            Assert.assertEquals(Resource.Success(dummy), it)
        }
    }
}