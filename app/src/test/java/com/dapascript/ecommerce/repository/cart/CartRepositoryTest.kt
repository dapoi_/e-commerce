package com.dapascript.ecommerce.repository.cart

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.cart.CartRepositoryImpl
import com.dapascript.ecommerce.data.source.local.db.CartDao
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class CartRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var cartDao: CartDao

    private lateinit var cartRepositoryImpl: CartRepositoryImpl

    private val productCartEntity = ProductCartEntity(
        "", "", "", "", "1", "", "", productQuantity = 1
    )

    @Before
    fun setUp() {
        cartRepositoryImpl = CartRepositoryImpl(cartDao)
    }

    @Test
    fun insertToCart() = runTest {
        Mockito.`when`(cartDao.insertToCart(productCartEntity)).thenReturn(1)

        cartRepositoryImpl.insertToCart(productCartEntity)
        Mockito.verify(cartDao).insertToCart(productCartEntity)
    }

    @Test
    fun insertAndUpdateSameStock() = runTest {
        val maxQuantity =
            productCartEntity.copy(productQuantity = productCartEntity.productStock.toInt())

        Mockito.`when`(cartDao.insertToCart(maxQuantity)).thenReturn(1L)
        Mockito.`when`(cartDao.getProductCartById(productCartEntity.id)).thenReturn(
            maxQuantity
        )

        cartRepositoryImpl.insertToCart(productCartEntity)

        Mockito.verify(
            cartDao, Mockito.never()
        ).updateProduct(maxQuantity.copy(productQuantity = maxQuantity.productQuantity + 1))
    }
}