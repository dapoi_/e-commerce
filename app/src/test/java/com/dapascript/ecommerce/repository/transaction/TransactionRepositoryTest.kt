package com.dapascript.ecommerce.repository.transaction

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.ExperimentalPagingApi
import com.dapascript.ecommerce.data.repository.main.transaction.TransactionRepositoryImpl
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.CoroutinesTestRule
import com.dapascript.ecommerce.helper.Dummy
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@ExperimentalPagingApi
@RunWith(MockitoJUnitRunner.Silent::class)
class TransactionRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Mock
    private lateinit var apiService: ApiService

    private lateinit var transactionRepository: TransactionRepositoryImpl

    @Before
    fun setUp() {
        transactionRepository =
            TransactionRepositoryImpl(apiService, coroutinesTestRule.testDispatcher)
    }

    @Test
    fun getTransaction() = runTest {
        val dataDummy = Dummy.dummyTransaction

        Mockito.`when`(apiService.getTransactionHistory()).thenReturn(dataDummy)

        transactionRepository.getTransactionRepository().drop(1).first().let {
            Assert.assertEquals(Resource.Success(dataDummy.data), it)
        }
    }
}