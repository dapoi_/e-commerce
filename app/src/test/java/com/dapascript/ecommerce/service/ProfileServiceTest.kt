package com.dapascript.ecommerce.service

import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.Dummy
import com.dapascript.ecommerce.helper.ErrorResponse
import com.dapascript.ecommerce.helper.MockResponseFileReader
import com.dapascript.ecommerce.helper.enqueueResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ProfileServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun shutDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun createProfile() = runTest {
        enqueueResponse("profile/success.json", 200, mockWebServer)
        val actual = apiService.createProfile(
            "".toRequestBody(),
            null
        )
        val expect = Dummy.createProfileSuccess
        assertEquals(expect, actual)
    }

    private fun HttpException.responseError(): ErrorResponse {
        val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
        val adapter = moshi.adapter(ErrorResponse::class.java)
        val responseJson = response()?.errorBody()?.string()
        val errorResponse = responseJson?.let { adapter.fromJson(it) }
        val code = errorResponse?.code ?: this.code()
        val message = errorResponse?.message ?: this.message()
        return ErrorResponse(code, message)
    }
}