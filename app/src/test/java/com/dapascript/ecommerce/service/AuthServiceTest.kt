package com.dapascript.ecommerce.service

import com.dapascript.ecommerce.data.source.remote.model.AuthRequest
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenRequest
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.helper.Dummy
import com.dapascript.ecommerce.helper.ErrorResponse
import com.dapascript.ecommerce.helper.enqueueResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.test.runTest
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class AuthServiceTest {

    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiService

    @Before
    fun startServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        val moshi = Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    @After
    fun shutDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun successSignUp() = runTest {
        enqueueResponse("signup/success.json", 200, mockWebServer)
        val authRequest = AuthRequest(
            "test@gmail.com",
            "123",
            "123"
        )
        val actualResponse = apiService.registerAccount(authRequest)
        val expectResponse = Dummy.signUpSuccess
        assertEquals(expectResponse, actualResponse)
    }

    @Test
    fun emailAlreadyTakenSignUp() = runTest {
        enqueueResponse("signup/emailSimiliar.json", 400, mockWebServer)
        val authRequest = AuthRequest(
            "test@gmail.com",
            "123",
            "123"
        )

        try {
            apiService.registerAccount(authRequest)
        } catch (e: HttpException) {
            assertEquals(Dummy.signUpEmailTaken, e.responseError())
        }
    }

    @Test
    fun signInSuccess() = runTest {
        enqueueResponse("signin/success.json", 200, mockWebServer)
        val authRequest = AuthRequest(
            "test@gmail.com",
            "123",
            "123"
        )
        val actualResponse = apiService.loginAccount(authRequest)
        val expectedResponse = Dummy.signInSuccess
        assertEquals(expectedResponse, actualResponse)
    }

    @Test
    fun invalidSignIn() = runTest {
        enqueueResponse("signin/loginInvalid.json", 400, mockWebServer)
        val authRequest = AuthRequest(
            "test@gmail.com",
            "123",
            "123"
        )

        try {
            apiService.loginAccount(authRequest)
        } catch (e: HttpException) {
            assertEquals(Dummy.signInInvalid, e.responseError())
        }
    }

    @Test
    fun refreshToken() = runTest {
        enqueueResponse("signup/success.json", 200, mockWebServer)

        val actual = apiService.refreshToken(RefreshTokenRequest(""))
        val expect = Dummy.refreshTokenSuccess
        assertEquals(expect, actual)
    }

    private fun HttpException.responseError(): ErrorResponse {
        val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
        val adapter = moshi.adapter(ErrorResponse::class.java)
        val responseJson = response()?.errorBody()?.string()
        val errorResponse = responseJson?.let { adapter.fromJson(it) }
        val code = errorResponse?.code ?: this.code()
        val message = errorResponse?.message ?: this.message()
        return ErrorResponse(code, message)
    }
}