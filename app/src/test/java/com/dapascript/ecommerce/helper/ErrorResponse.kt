package com.dapascript.ecommerce.helper

data class ErrorResponse(
    val code: Int,
    val message: String
)
