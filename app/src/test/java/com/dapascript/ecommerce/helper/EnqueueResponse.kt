package com.dapascript.ecommerce.helper

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.SocketPolicy

fun enqueueResponse(
    fileName: String,
    code: Int,
    mockWebServer: MockWebServer,
    socketPolicy: SocketPolicy? = null
) {
    val reader = MockResponseFileReader(fileName)
    val mockResponse = MockResponse()
        .setResponseCode(code)
        .setBody(reader.content)

    socketPolicy?.let { mockResponse.setSocketPolicy(it) }
    mockWebServer.enqueue(mockResponse)
}