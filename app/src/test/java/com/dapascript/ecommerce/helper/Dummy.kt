package com.dapascript.ecommerce.helper

import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.data.source.remote.model.DetailProductResponse
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponseItem
import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.LoginResponseData
import com.dapascript.ecommerce.data.source.remote.model.ProductResponse
import com.dapascript.ecommerce.data.source.remote.model.ProductResponseItem
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponseData
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenResponse
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenResponseData
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponseData
import com.dapascript.ecommerce.data.source.remote.model.SearchResponse
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryResponse

object Dummy {

    val signUpSuccess = RegisterResponse(
        code = 200,
        message = "OK",
        data = RegisterResponseData(
            accessToken = "",
            refreshToken = "",
            expiresAt = 0
        )
    )

    val signUpEmailTaken = ErrorResponse(
        code = 400,
        message = "Email is already taken"
    )

    val signInSuccess = LoginResponse(
        code = 200,
        message = "OK",
        data = LoginResponseData(
            userName = "",
            userImage = "",
            accessToken = "",
            refreshToken = "",
            expiresAt = 0
        )
    )

    val signInInvalid = ErrorResponse(
        code = 400,
        message = "Email or password is not valid"
    )

    val createProfileSuccess = ProfileResponse(
        code = 200,
        message = "OK",
        data = ProfileResponseData(
            userName = "",
            userImage = ""
        )
    )

    val getProductSuccess = ProductResponse(
        code = 200,
        message = "OK",
        data = ProductResponseItem(
            itemsPerPage = 1,
            currentItemCount = 1,
            pageIndex = 1,
            totalPages = 1,
            items = emptyList()
        )
    )

    val getDetailProductSuccess = DetailProductResponse(
        code = 200,
        message = "OK",
        data = DetailItem(
            emptyList(),
            "",
            "",
            0,
            "",
            "",
            0,
            0,
            emptyList(),
            0,
            0.0,
            "",
            0,
            0,
        )
    )

    val searchResult = SearchResponse(
        code = 200,
        message = "OK",
        data = emptyList()
    )

    val fulfillmentSuccess = FulfillmentResponse(
        code = 200,
        message = "OK",
        data = FulfillmentResponseItem(
            "", 0, "", "", "", true
        )
    )

    val ratingSuccess = RatingResponse(
        200, "OK"
    )

    val transactionSuccess = TransactionHistoryResponse(
        200, emptyList(), "OK"
    )

    val refreshTokenSuccess = RefreshTokenResponse(
        code = 200,
        message = "OK",
        data = RefreshTokenResponseData(
            accessToken = "",
            refreshToken = "",
            expiresAt = 0
        )
    )

    val dummyRegister = RegisterResponse(
        code = 200,
        message = "OK",
        data = RegisterResponseData(
            "", 0, ""
        )
    )

    val dummyLogin = LoginResponse(
        code = 200,
        message = "OK",
        data = LoginResponseData(
            "", "", "", 0, ""
        )
    )

    val dummyProfile = ProfileResponse(
        code = 200,
        message = "OK",
        data = ProfileResponseData(
            "", ""
        )
    )

    val dummyTransaction = TransactionHistoryResponse(
        code = 200,
        message = "OK",
        data = emptyList()
    )

    val dummyProductResponse = ProductResponse(
        code = 200,
        message = "OK",
        data = ProductResponseItem(
            0, 0, 0, 0, emptyList()
        )
    )

    fun dummyProductEntity(): List<ProductEntity> {
        val listProduct = ArrayList<ProductEntity>()
        for (i in 1..10) {
            val product = ProductEntity(
                "", "", "", 0, "", "", "", ""
            )

            listProduct.add(product)
        }

        return listProduct
    }
}