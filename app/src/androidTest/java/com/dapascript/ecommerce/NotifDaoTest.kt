package com.dapascript.ecommerce

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dapascript.ecommerce.data.source.local.db.NotifDao
import com.dapascript.ecommerce.data.source.local.db.ProductDB
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class NotifDaoTest {

    private lateinit var db: ProductDB
    private lateinit var notifDao: NotifDao

    @Before
    fun createDB() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ProductDB::class.java
        ).build()
        notifDao = db.notifDao()
    }

    @Test
    fun insertAndGetNotif() = runTest {
        val notifEntity = Dummy.notifEntity()
        notifDao.insertNotif(notifEntity)

        val getNotif = notifDao.getAllNotif().first()
        TestCase.assertEquals(getNotif, listOf(notifEntity))
    }

    @Test
    fun updateStateNotif() = runTest {
        val notifEntity = Dummy.notifEntity()
        val updated = notifEntity.copy(isRead = true)
        notifDao.insertNotif(notifEntity)
        notifDao.updateStateNotif(updated)

        val getNotif = notifDao.getAllNotif().first()
        TestCase.assertEquals(getNotif, listOf(updated))
    }

    @Test
    fun deleteAllNotif() = runTest {
        val notifEntity = Dummy.notifEntity()
        notifDao.insertNotif(notifEntity)

        notifDao.deleteAllNotif()
        val getNotif = notifDao.getAllNotif().first()
        TestCase.assertEquals(getNotif, emptyList<NotifEntity>())
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db.close()
    }

    object Dummy {
        fun notifEntity(): NotifEntity {
            return NotifEntity(
                1, "", "", "", "", "", ""
            )
        }
    }
}