package com.dapascript.ecommerce

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dapascript.ecommerce.data.preference.DataStorePreference
import junit.framework.TestCase
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

@RunWith(AndroidJUnit4::class)
class DataStorePreferenceTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var dataStore: DataStorePreference

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        dataStore = DataStorePreference(context)
    }

    @Test
    fun saveAndGetOnboardingState() = runTest {
        dataStore.saveOnboardingState(true)

        val isOnboardDone = dataStore.getOnboardingState.getOrAwaitValue()
        TestCase.assertEquals(true, isOnboardDone)
    }

    @Test
    fun saveAndGetUsername() = runTest {
        dataStore.saveUserName("Daffa")

        val getUsername = dataStore.getUserName.getOrAwaitValue()
        TestCase.assertEquals("Daffa", getUsername)
    }

    @Test
    fun saveAndGetAccessToken() = runTest {
        dataStore.saveAccessToken("123")

        val getAccessToken = dataStore.getAllToken.asLiveData().getOrAwaitValue().first
        TestCase.assertEquals("123", getAccessToken)
    }

    @Test
    fun saveAndGetRefreshToken() = runTest {
        dataStore.saveRefreshToken("456")

        val getRefreshToken = dataStore.getAllToken.asLiveData().getOrAwaitValue().second
        TestCase.assertEquals("456", getRefreshToken)
    }

    @Test
    fun signOut() = runTest {
        val signOut = dataStore.signOut()

        TestCase.assertEquals(Unit, signOut)
    }

    fun <T> LiveData<T>.getOrAwaitValue(
        time: Long = 2,
        timeUnit: TimeUnit = TimeUnit.SECONDS
    ): T {
        var data: T? = null
        val latch = CountDownLatch(1)
        val observer = object : Observer<T> {
            override fun onChanged(value: T) {
                data = value
                latch.countDown()
                this@getOrAwaitValue.removeObserver(this)
            }
        }

        this.observeForever(observer)

        // Don't wait indefinitely if the LiveData is not set.
        if (!latch.await(time, timeUnit)) {
            throw TimeoutException("LiveData value was never set.")
        }

        @Suppress("UNCHECKED_CAST")
        return data as T
    }
}