package com.dapascript.ecommerce

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dapascript.ecommerce.data.source.local.db.ProductDB
import com.dapascript.ecommerce.data.source.local.db.WishlistDao
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class WishlistDaoTest {

    private lateinit var db: ProductDB
    private lateinit var wishlistDao: WishlistDao

    @Before
    fun createDB() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ProductDB::class.java
        ).build()
        wishlistDao = db.wishlistDao()
    }

    @Test
    fun insertAndGetWishlist() = runTest {
        val productWishlist = Dummy.productWishlistEntity()
        wishlistDao.insertToWishlist(productWishlist)

        val getWishlist = wishlistDao.getAllProductWishlist().first()
        TestCase.assertEquals(getWishlist, listOf(productWishlist))
    }

    @Test
    fun deleteWishlistById() = runTest {
        val wishlistEntity = Dummy.productWishlistEntity()
        wishlistDao.insertToWishlist(wishlistEntity)

        wishlistDao.deleteWishlistById("1")
        val getWishlist = wishlistDao.getAllProductWishlist().first()
        TestCase.assertEquals(getWishlist, listOf<ProductWishlistEntity>())
    }

    @Test
    fun deleteWishlistFromDetail() = runTest {
        val wishlistEntity = Dummy.productWishlistEntity()
        wishlistDao.insertToWishlist(wishlistEntity)

        wishlistDao.deleteWishlistFromDetail(wishlistEntity)
        val getWishlist = wishlistDao.getAllProductWishlist().first()
        TestCase.assertEquals(getWishlist, emptyList<ProductWishlistEntity>())
    }

    @Test
    fun checkProductWishlist() = runTest {
        val wishlistEntity = Dummy.productWishlistEntity()
        wishlistDao.insertToWishlist(wishlistEntity)

        wishlistDao.checkProductWishlist("1")
        val getWishlist = wishlistDao.getAllProductWishlist().first()
        TestCase.assertEquals(getWishlist, listOf(wishlistEntity))
    }

    @Test
    fun deleteAllFromWishlist() = runTest {
        val wishlistEntity = Dummy.productWishlistEntity()
        wishlistDao.insertToWishlist(wishlistEntity)

        wishlistDao.deleteAllProductWishlist()
        val getWishlist = wishlistDao.getAllProductWishlist().first()
        TestCase.assertEquals(getWishlist, emptyList<ProductWishlistEntity>())
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db.close()
    }

    object Dummy {
        fun productWishlistEntity(): ProductWishlistEntity {
            return ProductWishlistEntity(
                "1", "", "", "", "", "", 1, "", "", ""
            )
        }
    }
}