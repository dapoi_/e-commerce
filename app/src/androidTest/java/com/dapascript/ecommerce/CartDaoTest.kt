package com.dapascript.ecommerce

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.dapascript.ecommerce.data.source.local.db.CartDao
import com.dapascript.ecommerce.data.source.local.db.ProductDB
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class CartDaoTest {

    private lateinit var db: ProductDB
    private lateinit var cartDao: CartDao

    @Before
    fun createDB() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, ProductDB::class.java
        ).build()
        cartDao = db.cartDao()
    }

    @Test
    fun insertGetAllCart() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.insertToCart(productCartEntity)

        val getAllCart = cartDao.getAllProductCart().first()
        TestCase.assertEquals(getAllCart, listOf(productCartEntity))
    }

    @Test
    fun insertGetSelectedProduct() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.insertToCart(productCartEntity)

        val getSelectedCart = cartDao.getSelectedProduct().first()
        TestCase.assertEquals(getSelectedCart, listOf(productCartEntity).filter { it.isSelected })
    }

    @Test
    fun insertGetProductById() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.insertToCart(productCartEntity)

        val getProductById = cartDao.getProductCartById("1")
        TestCase.assertEquals(getProductById, productCartEntity)
    }

    @Test
    fun updateProduct() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        val updated = productCartEntity.copy(isSelected = true)
        cartDao.insertToCart(productCartEntity)
        cartDao.updateProduct(updated)

        val getCart = cartDao.getAllProductCart().first()
        TestCase.assertEquals(getCart, listOf(updated))
    }

    @Test
    fun updateAllProduct() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.updateAllSelectedProduct(true)

        val getCart = cartDao.getAllProductCart().first()
        TestCase.assertEquals(getCart, listOf(productCartEntity).filter { it.isSelected })
    }

    @Test
    fun deleteCartById() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.insertToCart(productCartEntity)

        cartDao.deleteProductFromCart("1")
        val getCart = cartDao.getAllProductCart().first()
        TestCase.assertEquals(getCart, listOf<ProductCartEntity>())
    }

    @Test
    fun deleteAllCart() = runTest {
        val productCartEntity = Dummy.productCartEntity()
        cartDao.insertToCart(productCartEntity)

        cartDao.deleteAllProduct()
        val getCart = cartDao.getAllProductCart().first()
        TestCase.assertEquals(getCart, emptyList<ProductCartEntity>())
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db.close()
    }

    object Dummy {
        fun productCartEntity(): ProductCartEntity {
            return ProductCartEntity(
                "1", "", "", "", "", "", "", 1
            )
        }
    }
}