package com.dapascript.ecommerce.data.source.local.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductCheckoutEntity(
    val id: String,
    val productImage: String,
    val productName: String,
    val productVariant: String,
    val productStock: String,
    val productPrice: String,
    var productQuantity: Int = 1,
    var isSelected: Boolean = false,
) : Parcelable
