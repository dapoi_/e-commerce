package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class PaymentData(

    @Json(name = "item")
    val item: List<PaymentItem>,

    @Json(name = "title")
    val title: String
)

data class PaymentItem(

    @Json(name = "image")
    val image: String,

    @Json(name = "label")
    val label: String,

    @Json(name = "status")
    val status: Boolean
)
