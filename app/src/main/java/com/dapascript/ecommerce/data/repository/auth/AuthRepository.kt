package com.dapascript.ecommerce.data.repository.auth

import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface AuthRepository {

    fun registerAccount(
        email: String,
        password: String,
    ): Flow<Resource<RegisterResponse>>

    fun loginAccount(
        email: String,
        password: String,
    ): Flow<Resource<LoginResponse>>
}