package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class DetailProductResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: DetailItem,

    @Json(name = "message")
    val message: String
)

data class DetailItem(

    @Json(name = "image")
    val image: List<String>,

    @Json(name = "productId")
    val productId: String,

    @Json(name = "description")
    val description: String,

    @Json(name = "totalRating")
    val totalRating: Int,

    @Json(name = "store")
    val store: String,

    @Json(name = "productName")
    val productName: String,

    @Json(name = "totalSatisfaction")
    val totalSatisfaction: Int,

    @Json(name = "sale")
    val sale: Int,

    @Json(name = "productVariant")
    val productVariant: List<ProductVariantItem>,

    @Json(name = "stock")
    val stock: Int,

    @Json(name = "productRating")
    val productRating: Any,

    @Json(name = "brand")
    val brand: String,

    @Json(name = "productPrice")
    val productPrice: Int,

    @Json(name = "totalReview")
    val totalReview: Int
)

data class ProductVariantItem(

    @Json(name = "variantPrice")
    val variantPrice: Int,

    @Json(name = "variantName")
    val variantName: String
)
