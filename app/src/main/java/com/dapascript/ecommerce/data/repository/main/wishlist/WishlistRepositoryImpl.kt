package com.dapascript.ecommerce.data.repository.main.wishlist

import com.dapascript.ecommerce.data.source.local.db.WishlistDao
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WishlistRepositoryImpl @Inject constructor(
    private val wishlistDao: WishlistDao
) : WishlistRepository {

    override fun getAllWishlist(): Flow<List<ProductWishlistEntity>> {
        return wishlistDao.getAllProductWishlist()
    }

    override suspend fun deleteWishlistId(id: String) {
        wishlistDao.deleteWishlistById(id)
    }

    override suspend fun deleteAllWishlist() {
        wishlistDao.deleteAllProductWishlist()
    }
}