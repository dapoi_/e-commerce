package com.dapascript.ecommerce.data.source.remote.model

data class RatingRequest(
    val invoiceId: String,
    val rating: Int? = null,
    val review: String? = null
)
