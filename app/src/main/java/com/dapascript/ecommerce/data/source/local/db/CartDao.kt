package com.dapascript.ecommerce.data.source.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CartDao {

    @Query("SELECT * FROM product_cart")
    fun getAllProductCart(): Flow<List<ProductCartEntity>>

    @Query("SELECT * FROM product_cart WHERE isSelected = 1")
    fun getSelectedProduct(): Flow<List<ProductCartEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertToCart(cartProduct: ProductCartEntity): Long

    @Query("SELECT * FROM product_cart WHERE id = :id")
    suspend fun getProductCartById(id: String): ProductCartEntity

    @Update
    suspend fun updateProduct(cartProduct: ProductCartEntity)

    @Query("UPDATE product_cart SET isSelected = :isSelected")
    suspend fun updateAllSelectedProduct(isSelected: Boolean)

    @Query("DELETE FROM product_cart WHERE id = :id")
    suspend fun deleteProductFromCart(id: String)

    @Query("DELETE FROM product_cart")
    suspend fun deleteAllProduct()
}