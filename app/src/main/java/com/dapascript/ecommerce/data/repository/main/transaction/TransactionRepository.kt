package com.dapascript.ecommerce.data.repository.main.transaction

import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface TransactionRepository {

    fun getTransactionRepository(): Flow<Resource<List<TransactionHistoryData>>>
}