package com.dapascript.ecommerce.data.analytics

import android.os.Bundle
import androidx.core.os.bundleOf
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponseItem
import com.dapascript.ecommerce.presentation.viewmodel.StoreViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AnalyticsManager @Inject constructor(
    private val firebaseAnalytics: FirebaseAnalytics
) {

    fun logSignInEvent(email: String) {
        val event = FirebaseAnalytics.Event.LOGIN
        val param = bundleOf(FirebaseAnalytics.Param.METHOD to email)
        logEvent(event, param)
    }

    fun logSignUpEvent(email: String) {
        val event = FirebaseAnalytics.Event.SIGN_UP
        val param = bundleOf(FirebaseAnalytics.Param.METHOD to email)
        logEvent(event, param)
    }

    fun logSearchResultEvent(query: String) {
        val event = FirebaseAnalytics.Event.SEARCH
        val param = bundleOf(FirebaseAnalytics.Param.SEARCH_TERM to query)
        logEvent(event, param)
    }

    fun selectItemFilterEvent(filter: StoreViewModel.FilterProduct) {
        val event = FirebaseAnalytics.Event.SELECT_ITEM
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_CATEGORY to filter.chipCategory,
            FirebaseAnalytics.Param.ITEM_NAME to filter.chipSort,
            FirebaseAnalytics.Param.PRICE to filter.minPrice,
            FirebaseAnalytics.Param.PRICE to filter.maxPrice
        )
        logEvent(event, param)
    }

    fun viewItemListingEvent(itemList: List<ProductEntity>) {
        val event = FirebaseAnalytics.Event.VIEW_ITEM_LIST
        val items = ArrayList<Bundle>()
        itemList.forEach {
            val bundle = bundleOf(
                FirebaseAnalytics.Param.ITEM_ID to it.productId,
                FirebaseAnalytics.Param.ITEM_NAME to it.name,
                FirebaseAnalytics.Param.ITEM_BRAND to it.brand,
                FirebaseAnalytics.Param.PRICE to it.price.toString()
            )
            items.add(bundle)
        }

        val param = bundleOf(
            FirebaseAnalytics.Param.ITEMS to items
        )
        logEvent(event, param)
    }

    fun selectItemProductEvent(itemEntity: ProductEntity) {
        val event = FirebaseAnalytics.Event.SELECT_ITEM
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to itemEntity.productId,
            FirebaseAnalytics.Param.ITEM_NAME to itemEntity.name,
            FirebaseAnalytics.Param.ITEM_BRAND to itemEntity.brand,
            FirebaseAnalytics.Param.PRICE to itemEntity.price.toString()
        )
        logEvent(event, param)
    }

    fun viewItemEvent(detailItem: DetailItem) {
        val event = FirebaseAnalytics.Event.VIEW_ITEM
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to detailItem.productId,
            FirebaseAnalytics.Param.ITEM_NAME to detailItem.productName,
            FirebaseAnalytics.Param.ITEM_BRAND to detailItem.brand,
            FirebaseAnalytics.Param.PRICE to detailItem.productPrice.toString()
        )
        logEvent(event, param)
    }

    fun addToCartEvent(detailItem: DetailItem) {
        val event = FirebaseAnalytics.Event.ADD_TO_CART
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to detailItem.productId,
            FirebaseAnalytics.Param.ITEM_NAME to detailItem.productName,
            FirebaseAnalytics.Param.ITEM_BRAND to detailItem.brand,
            FirebaseAnalytics.Param.PRICE to detailItem.productPrice.toString()
        )
        logEvent(event, param)
    }

    fun removeFromCartEvent(cart: ProductCartEntity) {
        val event = FirebaseAnalytics.Event.REMOVE_FROM_CART
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to cart.id,
            FirebaseAnalytics.Param.ITEM_NAME to cart.productName,
            FirebaseAnalytics.Param.ITEM_BRAND to cart.brand,
            FirebaseAnalytics.Param.PRICE to cart.productPrice
        )
        logEvent(event, param)
    }

    fun viewCartEvent(product: List<ProductCartEntity>) {
        val event = FirebaseAnalytics.Event.VIEW_CART
        val items = ArrayList<Bundle>()
        product.forEach {
            val bundle = bundleOf(
                FirebaseAnalytics.Param.ITEM_ID to it.id,
                FirebaseAnalytics.Param.ITEM_NAME to it.productName,
                FirebaseAnalytics.Param.ITEM_BRAND to it.brand,
                FirebaseAnalytics.Param.PRICE to it.productPrice
            )
            items.add(bundle)
        }
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEMS to items
        )
        logEvent(event, param)
    }

    fun addToWishlistEvent(productWishlistEntity: ProductWishlistEntity) {
        val event = FirebaseAnalytics.Event.ADD_TO_WISHLIST
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to productWishlistEntity.id,
            FirebaseAnalytics.Param.ITEM_NAME to productWishlistEntity.productName,
            FirebaseAnalytics.Param.ITEM_BRAND to productWishlistEntity.brand,
            FirebaseAnalytics.Param.PRICE to productWishlistEntity.productPrice
        )
        logEvent(event, param)
    }

    fun beginCheckoutEvent(items: DetailItem) {
        val event = FirebaseAnalytics.Event.BEGIN_CHECKOUT
        val param = bundleOf(
            FirebaseAnalytics.Param.ITEM_ID to items.productId,
            FirebaseAnalytics.Param.ITEM_NAME to items.productName,
            FirebaseAnalytics.Param.ITEM_BRAND to items.brand,
            FirebaseAnalytics.Param.PRICE to items.productPrice.toString()
        )
        logEvent(event, param)
    }

    fun addPaymentInfoEvent(payment: String) {
        val event = FirebaseAnalytics.Event.ADD_PAYMENT_INFO
        val param = bundleOf(
            FirebaseAnalytics.Param.PAYMENT_TYPE to payment
        )
        logEvent(event, param)
    }

    fun purchaseEvent(items: FulfillmentResponseItem) {
        val event = FirebaseAnalytics.Event.PURCHASE
        val param = bundleOf(
            FirebaseAnalytics.Param.TRANSACTION_ID to items.invoiceId,
            FirebaseAnalytics.Param.PRICE to items.total.toString(),
            FirebaseAnalytics.Param.PAYMENT_TYPE to items.payment,
            FirebaseAnalytics.Param.END_DATE to items.date
        )
        logEvent(event, param)
    }

    fun buttonClickEvent(buttonName: String) {
        val event = "button_click"
        val param = bundleOf("button_name" to buttonName)
        logEvent(event, param)
    }

    private fun logEvent(eventName: String, params: Bundle = Bundle()) {
        firebaseAnalytics.logEvent(eventName, params)
    }
}