package com.dapascript.ecommerce.data.source.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotifDao {

    // insert notif to db
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertNotif(notifEntity: NotifEntity)

    // get all notif from db as list
    @Query("SELECT * FROM notif")
    fun getAllNotif(): Flow<List<NotifEntity>>

    // update notif
    @Update
    suspend fun updateStateNotif(notifEntity: NotifEntity)

    // delete all notif
    @Query("DELETE FROM notif")
    suspend fun deleteAllNotif()
}