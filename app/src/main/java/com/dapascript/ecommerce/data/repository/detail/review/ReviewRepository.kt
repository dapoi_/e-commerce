package com.dapascript.ecommerce.data.repository.detail.review

import com.dapascript.ecommerce.data.source.remote.model.ReviewsItem
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface ReviewRepository {

    fun getReviews(productId: String): Flow<Resource<List<ReviewsItem>>>
}