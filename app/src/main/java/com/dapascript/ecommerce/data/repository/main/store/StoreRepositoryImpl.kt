package com.dapascript.ecommerce.data.repository.main.store

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.data.source.remote.PagingSource
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StoreRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : StoreRepository {

    override fun getProducts(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?,
        limit: Int?,
        page: Int?
    ): Flow<PagingData<ProductEntity>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                initialLoadSize = 10,
                prefetchDistance = 1
            ),
            pagingSourceFactory = {
                PagingSource(apiService, search, brand, lowest, highest, sort)
            }
        ).flow
    }
}