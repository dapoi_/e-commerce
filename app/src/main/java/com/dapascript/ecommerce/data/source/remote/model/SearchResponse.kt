package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class SearchResponse(
    @Json(name = "code")
    val code: Int,

    @Json(name = "message")
    val message: String,

    @Json(name = "data")
    val data: List<String>
)
