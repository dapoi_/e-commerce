package com.dapascript.ecommerce.data.repository.notif

import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import kotlinx.coroutines.flow.Flow

interface NotifRepository {

    fun getAllNotif(): Flow<List<NotifEntity>>

    suspend fun readNotif(notifEntity: NotifEntity, isRead: Boolean)

    suspend fun deleteAllNotif()
}