package com.dapascript.ecommerce.data.repository.profile

import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface ProfileRepository {

    fun createProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?
    ): Flow<Resource<ProfileResponse>>
}