package com.dapascript.ecommerce.data.source.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity

@Database(
    entities = [ProductCartEntity::class, ProductWishlistEntity::class, NotifEntity::class],
    version = 1,
    exportSchema = false
)
abstract class ProductDB : RoomDatabase() {
    abstract fun cartDao(): CartDao
    abstract fun wishlistDao(): WishlistDao
    abstract fun notifDao(): NotifDao
}