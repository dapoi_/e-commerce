package com.dapascript.ecommerce.data.source.remote.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

data class FulfillmentResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: FulfillmentResponseItem,

    @Json(name = "message")
    val message: String
)

@Parcelize
data class FulfillmentResponseItem(

    @Json(name = "date")
    val date: String,

    @Json(name = "total")
    val total: Int,

    @Json(name = "invoiceId")
    val invoiceId: String,

    @Json(name = "payment")
    val payment: String,

    @Json(name = "time")
    val time: String,

    @Json(name = "status")
    val status: Boolean
) : Parcelable
