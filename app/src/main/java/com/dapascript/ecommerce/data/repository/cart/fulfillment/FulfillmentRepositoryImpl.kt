package com.dapascript.ecommerce.data.repository.cart.fulfillment

import com.dapascript.ecommerce.data.source.remote.model.FulfillmentItem
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentRequest
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.RatingRequest
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FulfillmentRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : FulfillmentRepository {

    override fun postFullfillment(
        payment: String,
        productId: String,
        variantName: String,
        quantity: Int
    ): Flow<Resource<FulfillmentResponse>> = flow {
        emit(Resource.Loading)

        try {
            val fulfillmentRequest = FulfillmentRequest(
                payment,
                listOf(FulfillmentItem(productId, variantName, quantity))
            )
            val response = apiService.postFulfillment(fulfillmentRequest)
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)

    override fun rateProduct(
        invoiceId: String,
        rating: Int?,
        review: String?
    ): Flow<Resource<RatingResponse>> = flow {
        emit(Resource.Loading)

        try {
            val rateRequest = RatingRequest(invoiceId, rating, review)
            val response = apiService.ratingProduct(rateRequest)
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}