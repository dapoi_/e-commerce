package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class ReviewProductResponse(

	@Json(name="code")
	val code: Int,

	@Json(name="data")
	val data: List<ReviewsItem>,

	@Json(name="message")
	val message: String
)

data class ReviewsItem(

	@Json(name="userImage")
	val userImage: String,

	@Json(name="userName")
	val userName: String,

	@Json(name="userReview")
	val userReview: String,

	@Json(name="userRating")
	val userRating: Int
)
