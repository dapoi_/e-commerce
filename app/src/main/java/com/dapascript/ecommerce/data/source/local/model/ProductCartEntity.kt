package com.dapascript.ecommerce.data.source.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_cart")
data class ProductCartEntity(
    @PrimaryKey val id: String,
    val productImage: String,
    val productName: String,
    val productVariant: String,
    val productStock: String,
    val productPrice: String,
    val brand: String,
    @ColumnInfo(name = "productQuantity", defaultValue = "1") val productQuantity: Int = 1,
    @ColumnInfo(name = "isSelected", defaultValue = "false") val isSelected: Boolean = false,
)
