package com.dapascript.ecommerce.data.repository.cart

import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import kotlinx.coroutines.flow.Flow

interface CartRepository {

    fun getCartProduct(): Flow<List<ProductCartEntity>>

    fun getSelectedProduct(): Flow<List<ProductCartEntity>>

    suspend fun insertToCart(cartProduct: ProductCartEntity)

    suspend fun updateProduct(cartProduct: ProductCartEntity)

    suspend fun updateAllSelectedProduct(isSelected: Boolean)

    suspend fun deleteProduct(id: String)

    suspend fun deleteAllProduct()
}