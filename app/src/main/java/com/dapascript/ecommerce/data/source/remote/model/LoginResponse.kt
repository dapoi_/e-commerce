package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class LoginResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: LoginResponseData,

    @Json(name = "message")
    val message: String
)

data class LoginResponseData(

    @Json(name = "userName")
    val userName: String,

    @Json(name = "userImage")
    val userImage: String,

    @Json(name = "accessToken")
    val accessToken: String,

    @Json(name = "expiresAt")
    val expiresAt: Int,

    @Json(name = "refreshToken")
    val refreshToken: String
)
