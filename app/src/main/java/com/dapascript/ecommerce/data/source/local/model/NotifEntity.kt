package com.dapascript.ecommerce.data.source.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notif")
data class NotifEntity(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val title: String?,
    val desc: String?,
    val date: String?,
    val time: String?,
    val type: String?,
    val image: String?,
    var isRead: Boolean = false
)
