package com.dapascript.ecommerce.data.repository.detail.review

import com.dapascript.ecommerce.data.source.remote.model.ReviewsItem
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReviewRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ReviewRepository {

    override fun getReviews(productId: String): Flow<Resource<List<ReviewsItem>>> = flow {
        emit(Resource.Loading)

        try {
            val response = apiService.getReview(productId).data
            if (response.isNotEmpty()) {
                emit(Resource.Success(response))
            } else {
                emit(Resource.Error(Exception("No reviews found")))
            }
        } catch (e: Exception) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}