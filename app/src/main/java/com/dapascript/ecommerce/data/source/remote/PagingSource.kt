package com.dapascript.ecommerce.data.source.remote

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.data.source.remote.network.ApiService

class PagingSource(
    private val apiService: ApiService,
    private val search: String?,
    private val brand: String?,
    private val lowest: Int?,
    private val highest: Int?,
    private val sort: String?,
) : PagingSource<Int, ProductEntity>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductEntity> {
        return try {
            val page = params.key ?: INITIAL_PAGE_INDEX
            val responseData = apiService.getProduct(
                search = search,
                brand = brand,
                lowest = lowest,
                highest = highest,
                sort = sort,
                limit = params.loadSize,
                page = page
            )

            val productEntity = responseData.data.items.map {
                ProductEntity(
                    productId = it.productId,
                    name = it.productName,
                    image = it.image,
                    price = it.productPrice,
                    sellerName = it.store,
                    rating = it.productRating.toString(),
                    sold = it.sale.toString(),
                    brand = it.brand
                )
            }

            LoadResult.Page(
                data = productEntity,
                prevKey = if (page == 1) null else page - 1,
                nextKey = if (page == responseData.data.totalPages) null else page + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, ProductEntity>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    private companion object {
        const val INITIAL_PAGE_INDEX = 1
    }
}