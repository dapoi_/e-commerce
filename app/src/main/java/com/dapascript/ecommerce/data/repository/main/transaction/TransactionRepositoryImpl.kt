package com.dapascript.ecommerce.data.repository.main.transaction

import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TransactionRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : TransactionRepository {

    override fun getTransactionRepository(): Flow<Resource<List<TransactionHistoryData>>> = flow {
        emit(Resource.Loading)

        try {
            val response = apiService.getTransactionHistory().data
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}