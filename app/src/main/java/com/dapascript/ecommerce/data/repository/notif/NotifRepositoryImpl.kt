package com.dapascript.ecommerce.data.repository.notif

import com.dapascript.ecommerce.data.source.local.db.NotifDao
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotifRepositoryImpl @Inject constructor(
    private val notifDao: NotifDao
) : NotifRepository {

    override fun getAllNotif(): Flow<List<NotifEntity>> = notifDao.getAllNotif()

    override suspend fun readNotif(notifEntity: NotifEntity, isRead: Boolean) {
        notifEntity.isRead = isRead
        notifDao.updateStateNotif(notifEntity)
    }

    override suspend fun deleteAllNotif() = notifDao.deleteAllNotif()
}