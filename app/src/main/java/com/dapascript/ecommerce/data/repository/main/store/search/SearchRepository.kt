package com.dapascript.ecommerce.data.repository.main.store.search

import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface SearchRepository {

    fun searchProduct(query: String): Flow<Resource<List<String>?>>
}