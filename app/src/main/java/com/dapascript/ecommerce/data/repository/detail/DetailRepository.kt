package com.dapascript.ecommerce.data.repository.detail

import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface DetailRepository {

    fun getProduct(id: String): Flow<Resource<DetailItem>>

    suspend fun insertToWishlist(product: ProductWishlistEntity)

    suspend fun deleteWishlistFromDetail(product: ProductWishlistEntity)

    fun checkProductWishlist(id: String): Flow<Boolean>
}