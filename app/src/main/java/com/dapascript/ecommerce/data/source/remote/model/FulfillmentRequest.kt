package com.dapascript.ecommerce.data.source.remote.model

data class FulfillmentRequest(
    val payment: String,
    val items: List<FulfillmentItem>
)

data class FulfillmentItem(
    val productId: String,
    val variantName: String,
    val quantity: Int
)
