package com.dapascript.ecommerce.data.repository.auth

import com.dapascript.ecommerce.data.source.remote.model.AuthRequest
import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val firebaseMessaging: FirebaseMessaging,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : AuthRepository {

    override fun registerAccount(
        email: String,
        password: String,
    ): Flow<Resource<RegisterResponse>> = flow {
        emit(Resource.Loading)

        try {
            val firebaseToken = firebaseMessaging.token.await()
            val authRequest = AuthRequest(email, password, firebaseToken)
            val response = apiService.registerAccount(authRequest)
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)

    override fun loginAccount(
        email: String,
        password: String,
    ): Flow<Resource<LoginResponse>> = flow {
        emit(Resource.Loading)

        try {
            val firebaseToken = firebaseMessaging.token.await()
            val authRequest = AuthRequest(email, password, firebaseToken)
            val response = apiService.loginAccount(authRequest)
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}