package com.dapascript.ecommerce.data.source.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_wishlist")
data class ProductWishlistEntity(
    @PrimaryKey val id: String,
    val productImage: String,
    val productName: String,
    val productPrice: String,
    val sellerName: String,
    val rate: String,
    val totalSold: Int,
    val chipVariant: String,
    val stock: String,
    val brand: String
)
