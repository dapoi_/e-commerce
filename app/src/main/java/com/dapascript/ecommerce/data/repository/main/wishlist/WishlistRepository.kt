package com.dapascript.ecommerce.data.repository.main.wishlist

import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import kotlinx.coroutines.flow.Flow

interface WishlistRepository {

    fun getAllWishlist(): Flow<List<ProductWishlistEntity>>

    suspend fun deleteWishlistId(id: String)

    suspend fun deleteAllWishlist()
}