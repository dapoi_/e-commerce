package com.dapascript.ecommerce.data.repository.detail

import com.dapascript.ecommerce.data.source.local.db.WishlistDao
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DetailRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val wishlistDao: WishlistDao,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : DetailRepository {

    override fun getProduct(id: String): Flow<Resource<DetailItem>> = flow {
        emit(Resource.Loading)

        try {
            val response = apiService.getDetail(id).data
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)

    override suspend fun insertToWishlist(product: ProductWishlistEntity) {
        wishlistDao.insertToWishlist(product)
    }

    override suspend fun deleteWishlistFromDetail(product: ProductWishlistEntity) {
        wishlistDao.deleteWishlistFromDetail(product)
    }

    override fun checkProductWishlist(id: String): Flow<Boolean> =
        wishlistDao.checkProductWishlist(id)
}