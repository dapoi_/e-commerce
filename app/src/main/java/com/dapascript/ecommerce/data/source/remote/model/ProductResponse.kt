package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class ProductResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: ProductResponseItem,

    @Json(name = "message")
    val message: String
)

data class ProductResponseItem(

    @Json(name = "pageIndex")
    val pageIndex: Int,

    @Json(name = "itemsPerPage")
    val itemsPerPage: Int,

    @Json(name = "currentItemCount")
    val currentItemCount: Int,

    @Json(name = "totalPages")
    val totalPages: Int,

    @Json(name = "items")
    val items: List<ProductItem>
)

data class ProductItem(

    @Json(name = "image")
    val image: String,

    @Json(name = "sale")
    val sale: Int,

    @Json(name = "productId")
    val productId: String,

    @Json(name = "store")
    val store: String,

    @Json(name = "productRating")
    val productRating: Any,

    @Json(name = "brand")
    val brand: String,

    @Json(name = "productName")
    val productName: String,

    @Json(name = "productPrice")
    val productPrice: Int
)
