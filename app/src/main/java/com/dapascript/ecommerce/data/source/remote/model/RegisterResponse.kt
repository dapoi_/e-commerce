package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class RegisterResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: RegisterResponseData,

    @Json(name = "message")
    val message: String
)

data class RegisterResponseData(

    @Json(name = "accessToken")
    val accessToken: String,

    @Json(name = "expiresAt")
    val expiresAt: Int,

    @Json(name = "refreshToken")
    val refreshToken: String
)
