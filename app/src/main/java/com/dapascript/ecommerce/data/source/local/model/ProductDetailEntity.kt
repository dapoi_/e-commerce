package com.dapascript.ecommerce.data.source.local.model

data class ProductDetailEntity(
    val id: String,
    val productImage: String,
    val productName: String,
    val productVariant: String,
    val productStock: String,
    val productPrice: String,
)