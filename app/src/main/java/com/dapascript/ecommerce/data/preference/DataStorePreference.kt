package com.dapascript.ecommerce.data.preference

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.asLiveData
import androidx.lifecycle.distinctUntilChanged
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import javax.inject.Singleton

private val ONBOARDING_STATE = booleanPreferencesKey("onboarding_state")
private val ACCESS_TOKEN = stringPreferencesKey("save_token")
private val REFRESH_TOKEN = stringPreferencesKey("refresh_token")
private val USER_NAME = stringPreferencesKey("user_name")
private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "ecommerce")

@Singleton
class DataStorePreference @Inject constructor(@ApplicationContext context: Context) {

    private val dataStore = context.dataStore

    /**
     * save onboarding state
     */
    suspend fun saveOnboardingState(state: Boolean) {
        runBlocking {
            dataStore.edit { preferences ->
                preferences[ONBOARDING_STATE] = state
            }
        }
    }

    /**
     * save user name
     */
    suspend fun saveUserName(name: String) {
        runBlocking {
            dataStore.edit { preferences ->
                preferences[USER_NAME] = name
            }
        }
    }

    /**
     * save token
     */
    suspend fun saveAccessToken(token: String) {
        dataStore.edit { preferences ->
            preferences[ACCESS_TOKEN] = token
        }
    }

    /**
     * save refresh token
     */
    suspend fun saveRefreshToken(token: String) {
        dataStore.edit { preferences ->
            preferences[REFRESH_TOKEN] = token
        }
    }

    /**
     * get token
     */
    val getAllToken = dataStore.data.map { preference ->
        val accessToken = preference[ACCESS_TOKEN] ?: ""
        val refreshToken = preference[REFRESH_TOKEN] ?: ""
        Pair(accessToken, refreshToken)
    }

    val getAccessToken = dataStore.data.map { preference ->
        preference[ACCESS_TOKEN] ?: ""
    }.asLiveData().distinctUntilChanged()

    /**
     * get onboarding state
     */
    val getOnboardingState = dataStore.data.map { preference ->
        preference[ONBOARDING_STATE] ?: false
    }.asLiveData().distinctUntilChanged()

    /**
     * get username
     */
    val getUserName = dataStore.data.map { preference ->
        preference[USER_NAME] ?: ""
    }.asLiveData().distinctUntilChanged()

    /**
     * sign out
     */
    suspend fun signOut() {
        dataStore.edit { preferences ->
            preferences.remove(ACCESS_TOKEN)
            preferences.remove(REFRESH_TOKEN)
            preferences.remove(USER_NAME)
        }
    }
}