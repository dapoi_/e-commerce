package com.dapascript.ecommerce.data.source.remote.network

import com.dapascript.ecommerce.data.source.remote.model.AuthRequest
import com.dapascript.ecommerce.data.source.remote.model.DetailProductResponse
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentRequest
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.ProductResponse
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.data.source.remote.model.RatingRequest
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenRequest
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.data.source.remote.model.ReviewProductResponse
import com.dapascript.ecommerce.data.source.remote.model.SearchResponse
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    // Register account
    @POST("register")
    suspend fun registerAccount(
        @Body authRequest: AuthRequest,
    ): RegisterResponse

    // Login account
    @POST("login")
    suspend fun loginAccount(
        @Body authRequest: AuthRequest
    ): LoginResponse

    // Refresh token
    @POST("refresh")
    suspend fun refreshToken(
        @Body refreshTokenRequest: RefreshTokenRequest
    ): RefreshTokenResponse

    // Create Profile
    @Multipart
    @POST("profile")
    suspend fun createProfile(
        @Part("userName") userName: RequestBody,
        @Part userImage: MultipartBody.Part?
    ): ProfileResponse

    // Get Product
    @POST("products")
    suspend fun getProduct(
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?
    ): ProductResponse

    // Detail
    @GET("products/{id}")
    suspend fun getDetail(
        @Path("id") id: String
    ): DetailProductResponse

    // Review
    @GET("review/{id}")
    suspend fun getReview(
        @Path("id") id: String
    ): ReviewProductResponse

    // Search Product
    @POST("search")
    suspend fun searchProduct(
        @Query("query") query: String
    ): SearchResponse

    // Fulfillment
    @POST("fulfillment")
    suspend fun postFulfillment(
        @Body fulfillmentRequest: FulfillmentRequest
    ): FulfillmentResponse

    // Rating Product
    @POST("rating")
    suspend fun ratingProduct(
        @Body ratingRequest: RatingRequest
    ): RatingResponse

    // Transaction History
    @GET("transaction")
    suspend fun getTransactionHistory(): TransactionHistoryResponse
}