package com.dapascript.ecommerce.data.repository.main.store

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface StoreRepository {

    fun getProducts(
        search: String?,
        brand: String?,
        lowest: Int?,
        highest: Int?,
        sort: String?,
        limit: Int?,
        page: Int?
    ): Flow<PagingData<ProductEntity>>
}