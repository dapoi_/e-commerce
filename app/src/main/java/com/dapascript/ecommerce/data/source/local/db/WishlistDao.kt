package com.dapascript.ecommerce.data.source.local.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface WishlistDao {

    /**
     * Product Wishlist
     */
    @Query("SELECT * FROM product_wishlist")
    fun getAllProductWishlist(): Flow<List<ProductWishlistEntity>>

    @Query("DELETE FROM product_wishlist WHERE id = :id")
    suspend fun deleteWishlistById(id: String)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertToWishlist(product: ProductWishlistEntity)

    @Delete
    suspend fun deleteWishlistFromDetail(product: ProductWishlistEntity)

    // check if product wishlist is exist
    @Query("SELECT EXISTS(SELECT * FROM product_wishlist WHERE id = :id)")
    fun checkProductWishlist(id: String): Flow<Boolean>

    // delete all
    @Query("DELETE FROM product_wishlist")
    suspend fun deleteAllProductWishlist()
}