package com.dapascript.ecommerce.data.repository.main.store.search

import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : SearchRepository {

    override fun searchProduct(
        query: String
    ): Flow<Resource<List<String>?>> = flow {
        emit(Resource.Loading)

        try {
            val response = apiService.searchProduct(query)
            val result = response.data
            if (result.isNotEmpty()) {
                emit(Resource.Success(result))
            } else {
                emit(Resource.Success(null))
            }
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}