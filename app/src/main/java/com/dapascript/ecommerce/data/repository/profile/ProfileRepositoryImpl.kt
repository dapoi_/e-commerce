package com.dapascript.ecommerce.data.repository.profile

import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.di.IoDispatcher
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProfileRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : ProfileRepository {

    override fun createProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?,
    ): Flow<Resource<ProfileResponse>> = flow {
        emit(Resource.Loading)

        try {
            val response = apiService.createProfile(userName, userImage)
            emit(Resource.Success(response))
        } catch (e: Throwable) {
            emit(Resource.Error(e))
        }
    }.flowOn(ioDispatcher)
}