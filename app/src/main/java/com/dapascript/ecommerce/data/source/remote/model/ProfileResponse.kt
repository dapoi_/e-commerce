package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class ProfileResponse(

    @Json(name = "code")
    val code: Int,

    @Json(name = "data")
    val data: ProfileResponseData,

    @Json(name = "message")
    val message: String
)

data class ProfileResponseData(

    @Json(name = "userImage")
    val userImage: String,

    @Json(name = "userName")
    val userName: String
)
