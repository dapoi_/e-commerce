package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class TransactionHistoryResponse(

    @Json(name = "code") val code: Int,

    @Json(name = "data") val data: List<TransactionHistoryData>,

    @Json(name = "message") val message: String
)

data class TransactionHistoryData(

    @Json(name = "date") val date: String,

    @Json(name = "image") val image: String,

    @Json(name = "total") val total: Int,

    @Json(name = "review") val review: String? = null,

    @Json(name = "rating") val rating: Int? = null,

    @Json(name = "invoiceId") val invoiceId: String,

    @Json(name = "payment") val payment: String,

    @Json(name = "time") val time: String,

    @Json(name = "items") val items: List<TransactionHistoryItem>,

    @Json(name = "status") val status: Boolean,

    @Json(name = "name") val name: String
)

data class TransactionHistoryItem(

    @Json(name = "quantity") val quantity: Int,

    @Json(name = "productId") val productId: String,

    @Json(name = "variantName") val variantName: String
)
