package com.dapascript.ecommerce.data.repository.cart.fulfillment

import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.vo.Resource
import kotlinx.coroutines.flow.Flow

interface FulfillmentRepository {

    fun postFullfillment(
        payment: String,
        productId: String,
        variantName: String,
        quantity: Int
    ): Flow<Resource<FulfillmentResponse>>

    fun rateProduct(
        invoiceId: String,
        rating: Int?,
        review: String?
    ): Flow<Resource<RatingResponse>>
}