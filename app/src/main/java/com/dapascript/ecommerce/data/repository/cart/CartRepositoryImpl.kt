package com.dapascript.ecommerce.data.repository.cart

import com.dapascript.ecommerce.data.source.local.db.CartDao
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CartRepositoryImpl @Inject constructor(
    private val cartDao: CartDao
) : CartRepository {

    // get all item from cart
    override fun getCartProduct(): Flow<List<ProductCartEntity>> = cartDao.getAllProductCart()

    // get all selected item from cart
    override fun getSelectedProduct(): Flow<List<ProductCartEntity>> = cartDao.getSelectedProduct()

    // add item to cart
    override suspend fun insertToCart(cartProduct: ProductCartEntity) {
        val rowId = cartDao.insertToCart(cartProduct)
        val cardProductById = cartDao.getProductCartById(cartProduct.id)
        if (rowId == -1L && cardProductById.productQuantity < cartProduct.productStock.toInt()) {
            val updateCart =
                cartProduct.copy(productQuantity = cardProductById.productQuantity + 1)
            updateProduct(updateCart)
        }
    }

    // update each item action (remove/add quantity, checkbox state, and price)
    override suspend fun updateProduct(cartProduct: ProductCartEntity) =
        cartDao.updateProduct(cartProduct)

    // update all checkbox state
    override suspend fun updateAllSelectedProduct(isSelected: Boolean) =
        cartDao.updateAllSelectedProduct(isSelected)

    // delete item from cart
    override suspend fun deleteProduct(id: String) = cartDao.deleteProductFromCart(id)

    // delete all item from cart
    override suspend fun deleteAllProduct() = cartDao.deleteAllProduct()
}