package com.dapascript.ecommerce.data.source.remote.model

import com.squareup.moshi.Json

data class RefreshTokenRequest(
    @Json(name = "token")
    val token: String
)
