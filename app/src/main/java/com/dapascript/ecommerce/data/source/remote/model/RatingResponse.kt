package com.dapascript.ecommerce.data.source.remote.model

data class RatingResponse(
    val code: Int,
    val message: String
)
