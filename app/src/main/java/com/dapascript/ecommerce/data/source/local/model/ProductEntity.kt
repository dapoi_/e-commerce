package com.dapascript.ecommerce.data.source.local.model

data class ProductEntity(
    val productId: String,
    val image: String,
    val name: String,
    val price: Int,
    val sellerName: String,
    val rating: String,
    val sold: String,
    val brand: String
)
