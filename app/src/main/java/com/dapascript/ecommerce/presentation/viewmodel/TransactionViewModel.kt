package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.dapascript.ecommerce.data.repository.main.transaction.TransactionRepository
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(
    private val transactionRepository: TransactionRepository
) : ViewModel() {

    fun getTransactionResult(): LiveData<Resource<List<TransactionHistoryData>>> {
        return transactionRepository.getTransactionRepository().asLiveData()
    }
}