package com.dapascript.ecommerce.presentation.view.main.store

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentStoreBinding
import com.dapascript.ecommerce.presentation.adapter.product.LoadingStateAdapter
import com.dapascript.ecommerce.presentation.adapter.product.ProductListAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.main.store.bottomsheet.BottomSheetFragment
import com.dapascript.ecommerce.presentation.viewmodel.StoreViewModel
import com.dapascript.ecommerce.presentation.viewmodel.StoreViewModel.FilterProduct
import com.dapascript.ecommerce.utils.currencyIndonesia
import com.dapascript.ecommerce.utils.handleError
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class StoreFragment : BaseFragment<FragmentStoreBinding>(FragmentStoreBinding::inflate) {

    private lateinit var storeAdapter: ProductListAdapter
    private lateinit var storeLayoutManager: GridLayoutManager

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    private val storeViewModel: StoreViewModel by viewModels()
    private var callback: BottomSheetFragment.PassChip? = null
    private var filterChipData = mutableListOf<String>()

    private var minPriceGlobal: String? = null
    private var maxPriceGlobal: String? = null
    private var selectedSortGlobal: String? = null
    private var selectedCategoryGlobal: String? = null
    private var queryGlobal: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.apply {
            etSearch.setOnClickListener {
                val data = etSearch.text.toString()
                findNavController().navigate(
                    StoreFragmentDirections.actionStoreFragmentToSearchFragment(data)
                )
            }

            chipFilter.setOnClickListener { showBottomSheet() }

            ivChangeView.setOnClickListener { storeViewModel.isLinearLayout() }

            layoutError.btnError.setOnClickListener {
                storeViewModel.setListProduct(
                    FilterProduct(null, null, null, null, null)
                )
                storeViewModel.setStateChip(
                    FilterProduct(null, null, null, null)
                )

                filterChipData.clear()
                etSearch.setText("")
                minPriceGlobal = null
                maxPriceGlobal = null
                cgFilter.removeAllViews()
            }

            srlStore.apply {
                setOnRefreshListener {
                    isRefreshing = false
                    storeAdapter.refresh()
                }
            }

            callback = object : BottomSheetFragment.PassChip {
                override fun selectedData(
                    chip: Map<String, String>?,
                    minPrice: String?,
                    maxPrice: String?
                ) {
                    storeViewModel.setListProduct(
                        FilterProduct(
                            chip?.get("Urutkan"),
                            chip?.get("Kategori"),
                            minPrice,
                            maxPrice,
                            etSearch.text.toString()
                        )
                    )

                    storeViewModel.setStateChip(
                        FilterProduct(
                            chip?.get("Urutkan"),
                            chip?.get("Kategori"),
                            minPrice,
                            maxPrice
                        )
                    )
                }

                override fun reset() {
                    storeViewModel.setListProduct(
                        FilterProduct(null, null, null, null, queryGlobal)
                    )
                }
            }

            initAdapter()
            getSearchResult()
        }
    }

    private fun getSearchResult() {
        requireActivity().supportFragmentManager.setFragmentResultListener(
            SEARCH_KEY, viewLifecycleOwner
        ) { _, bundle ->
            queryGlobal = bundle.getString(SEARCH_RESULT)
            storeViewModel.setListProduct(
                FilterProduct(
                    selectedSortGlobal,
                    selectedCategoryGlobal,
                    minPriceGlobal,
                    maxPriceGlobal,
                    queryGlobal
                )
            )
            binding.etSearch.setText(queryGlobal)
        }
    }

    private fun initAdapter() {
        binding.rvStore.apply {
            val loadingStateAdapter = LoadingStateAdapter()
            storeAdapter = ProductListAdapter()
            storeAdapter.onClick = { product ->
                val id = product.productId
                findNavController().navigate(
                    StoreFragmentDirections.actionStoreFragmentToDetailProductFragment(id)
                )

                analyticsManager.selectItemProductEvent(product)
            }
            adapter = storeAdapter.withLoadStateFooter(footer = loadingStateAdapter)

            storeLayoutManager = GridLayoutManager(requireContext(), 1).apply {
                spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if ((position == storeAdapter.itemCount) && loadingStateAdapter.itemCount > 0) {
                            spanCount
                        } else {
                            1
                        }
                    }
                }
            }
            layoutManager = storeLayoutManager

            val isLinearLayoutObserver = { isLinearLayout: Boolean ->
                storeAdapter.setViewType(if (isLinearLayout) 0 else 1)
                binding.ivChangeView.setImageResource(if (isLinearLayout) R.drawable.ic_list else R.drawable.ic_grid)
                storeLayoutManager.spanCount = if (isLinearLayout) 1 else 2
            }

            storeViewModel.isLinearLayout.observe(viewLifecycleOwner, isLinearLayoutObserver)
        }

        storeAdapter.addLoadStateListener { loadState ->
            binding.apply {
                val isLinearLayout = storeViewModel.isLinearLayout.value
                val isRefreshing = loadState.refresh is LoadState.Loading
                val isNotLoading = loadState.refresh is LoadState.NotLoading
                val isError = loadState.refresh is LoadState.Error

                shimmerList.isVisible = isRefreshing && isLinearLayout == true
                shimmerGrid.isVisible = isRefreshing && isLinearLayout == false
                cgFilter.isVisible = !isRefreshing
                chipFilter.isVisible = !isRefreshing
                ivChangeView.isVisible = !isRefreshing
                rvStore.isVisible = isNotLoading
                layoutError.root.isVisible = isError

                if (isError) handleError(
                    layoutError,
                    (loadState.refresh as LoadState.Error).error,
                    requireContext()
                )
            }
        }

        storeAdapter.addOnPagesUpdatedListener {
            analyticsManager.viewItemListingEvent(storeAdapter.snapshot().items)
        }

        lifecycleScope.launch {
            storeViewModel.getListProducts.collect {
                storeAdapter.submitData(it)
            }
        }

        storeViewModel.filterProduct.observe(viewLifecycleOwner) {
            confirmFilter(it.chipSort, it.chipCategory, it.minPrice, it.maxPrice)
        }
    }

    private fun confirmFilter(
        chipSort: String? = null,
        chipCategory: String? = null,
        minPrice: String? = null,
        maxPrice: String? = null,
    ) {
        selectedSortGlobal = chipSort
        selectedCategoryGlobal = chipCategory
        minPriceGlobal = minPrice
        maxPriceGlobal = maxPrice

        // Menghapus chip sebelumnya dari grup chip
        binding.cgFilter.removeAllViews()
        filterChipData.clear()

        selectedCategoryGlobal?.let { addChipToGroup(it) }
        selectedSortGlobal?.let { addChipToGroup(it) }

        if (!minPriceGlobal.isNullOrEmpty()) {
            addChipToGroup("> ${currencyIndonesia(minPrice)}")
        }

        if (!maxPriceGlobal.isNullOrEmpty()) {
            addChipToGroup("< ${currencyIndonesia(maxPrice)}")
        }
    }

    private fun addChipToGroup(chipData: String) {
        val chipComponent = Chip(requireContext()).apply {
            text = chipData
        }

        binding.cgFilter.addView(chipComponent)
        filterChipData.add(chipData)
    }

    private fun showBottomSheet() {
        val chipSort = listOf(
            "Ulasan", "Penjualan", "Harga Terendah", "Harga Tertinggi"
        )
        val chipCategory = listOf(
            "Asus", "Lenovo", "Apple", "Dell"
        )
        val bundle = Bundle().apply {
            putStringArray("chipSort", chipSort.toTypedArray())
            putStringArray("chipCategory", chipCategory.toTypedArray())
            putStringArray("filterChipData", filterChipData.toTypedArray())
            putString("minPrice", minPriceGlobal)
            putString("maxPrice", maxPriceGlobal)
        }
        BottomSheetFragment(callback as BottomSheetFragment.PassChip).apply {
            arguments = bundle
        }.show(requireActivity().supportFragmentManager, "BottomSheetDialog")
    }

    companion object {
        const val SEARCH_RESULT = "search_result"
        const val SEARCH_KEY = "search_key"
    }
}