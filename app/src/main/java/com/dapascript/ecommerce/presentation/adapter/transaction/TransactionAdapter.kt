package com.dapascript.ecommerce.presentation.adapter.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.remote.model.TransactionHistoryData
import com.dapascript.ecommerce.databinding.ItemListTransactionBinding
import com.dapascript.ecommerce.utils.currencyIndonesia

class TransactionAdapter(
    private var onReviewClicked: ((TransactionHistoryData) -> Unit)? = null
) : ListAdapter<TransactionHistoryData, TransactionAdapter.TransactionViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        return TransactionViewHolder(
            ItemListTransactionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class TransactionViewHolder(
        private val binding: ItemListTransactionBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(product: TransactionHistoryData) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(product.image)
                    .into(ivProduct)

                tvProductName.text = product.name
                tvShoppingDate.text = product.date
                tvTotalItem.text = itemView.context.getString(
                    R.string.total_item,
                    product.items[0].quantity.toString()
                )
                tvTotalPrice.text = currencyIndonesia(product.total.toString())

                btnReviewProduct.isVisible =
                    (product.rating == null || product.rating == 0) && product.review.isNullOrEmpty()
            }
        }

        init {
            binding.btnReviewProduct.setOnClickListener {
                onReviewClicked?.invoke(getItem(bindingAdapterPosition))
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TransactionHistoryData>() {
            override fun areItemsTheSame(
                oldItem: TransactionHistoryData,
                newItem: TransactionHistoryData
            ): Boolean {
                return oldItem.items[0].productId == newItem.items[0].productId
            }

            override fun areContentsTheSame(
                oldItem: TransactionHistoryData,
                newItem: TransactionHistoryData
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}