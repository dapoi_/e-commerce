package com.dapascript.ecommerce.presentation.adapter.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.dapascript.ecommerce.data.source.remote.model.PaymentData
import com.dapascript.ecommerce.data.source.remote.model.PaymentItem
import com.dapascript.ecommerce.databinding.ItemListPaymentHeaderBinding

class PaymentAdapter(
    private var onItemClick: ((PaymentItem) -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var getListPayment = ArrayList<PaymentData>()

    fun setListPayment(listPayment: List<PaymentData>) {
        getListPayment.clear()
        getListPayment.addAll(listPayment)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PaymentViewHolder(
            ItemListPaymentHeaderBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = getListPayment.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PaymentViewHolder).bind(getListPayment[position])
    }

    inner class PaymentViewHolder(
        private val binding: ItemListPaymentHeaderBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(payment: PaymentData) {
            binding.apply {
                tvHeader.text = payment.title
                with(rvPaymentList) {
                    adapter = PaymentItemAdapter(
                        onItemClick = {
                            onItemClick?.invoke(it)
                        }
                    )
                    (adapter as PaymentItemAdapter).setListPaymentItem(payment.item)
                    setHasFixedSize(true)
                }

                divider.isVisible = absoluteAdapterPosition != getListPayment.size - 1
            }
        }
    }
}