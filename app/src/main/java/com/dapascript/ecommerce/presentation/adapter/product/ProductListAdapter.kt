package com.dapascript.ecommerce.presentation.adapter.product

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import com.dapascript.ecommerce.databinding.ItemGridProductBinding
import com.dapascript.ecommerce.databinding.ItemListProductBinding
import com.dapascript.ecommerce.utils.currencyIndonesia
import java.text.NumberFormat
import java.util.Locale

class ProductListAdapter : PagingDataAdapter<ProductEntity, ViewHolder>(DIFF_UTIL) {

    var onClick: ((ProductEntity) -> Unit)? = null

    private var viewType = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            0 -> ProductViewHolder(
                ItemListProductBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            else -> ProductGridViewHolder(
                ItemGridProductBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = getItem(position)
        if (product != null) {
            when (holder) {
                is ProductViewHolder -> holder.bind(product)
                is ProductGridViewHolder -> holder.bind(product)
            }

            holder.itemView.setOnClickListener {
                onClick?.invoke(product)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    fun setViewType(i: Int) {
        viewType = i
    }

    inner class ProductViewHolder(
        private val binding: ItemListProductBinding
    ) : ViewHolder(binding.root) {
        fun bind(product: ProductEntity) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(product.image)
                    .placeholder(R.drawable.ic_image_default)
                    .into(ivProduct)

                tvProductName.text = product.name
                tvProductPrice.text = currencyIndonesia(product.price.toString())
                tvSellerName.text = product.sellerName
                tvRatingSold.text = itemView.context.getString(
                    R.string.rating_sold,
                    product.rating,
                    product.sold
                )
            }
        }
    }

    inner class ProductGridViewHolder(
        private val binding: ItemGridProductBinding
    ) : ViewHolder(binding.root) {
        fun bind(product: ProductEntity) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(product.image)
                    .placeholder(R.drawable.ic_image_default)
                    .into(ivProduct)

                tvProductName.text = product.name
                tvProductPrice.text =
                    NumberFormat.getCurrencyInstance(Locale("id", "ID")).format(product.price)
                tvSellerName.text = product.sellerName
                tvRatingSold.text = itemView.context.getString(
                    R.string.rating_sold,
                    product.rating,
                    product.sold
                )
            }
        }
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<ProductEntity>() {
            override fun areItemsTheSame(oldItem: ProductEntity, newItem: ProductEntity): Boolean {
                return oldItem.productId == newItem.productId
            }

            override fun areContentsTheSame(
                oldItem: ProductEntity,
                newItem: ProductEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}