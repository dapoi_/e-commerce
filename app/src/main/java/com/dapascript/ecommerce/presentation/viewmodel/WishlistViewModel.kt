package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.data.repository.main.wishlist.WishlistRepository
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val wishlistRepository: WishlistRepository,
    private val cartRepository: CartRepository
) : ViewModel() {

    private var _wishlist = MutableLiveData<List<ProductWishlistEntity>>()
    val wishlist: LiveData<List<ProductWishlistEntity>> = _wishlist

    private var _isLinearLayout = MutableLiveData(true)
    val isLinearLayout: LiveData<Boolean> get() = _isLinearLayout

    private fun getAllWishlist() {
        viewModelScope.launch {
            wishlistRepository.getAllWishlist().collect {
                _wishlist.value = it
            }
        }
    }

    fun deleteWishlistId(id: String) {
        viewModelScope.launch { wishlistRepository.deleteWishlistId(id) }
    }

    fun isLinearLayout() {
        _isLinearLayout.value = !_isLinearLayout.value!!
    }

    fun insertToCart(productWishlist: ProductWishlistEntity) {
        val productCart = ProductCartEntity(
            id = productWishlist.id,
            productImage = productWishlist.productImage,
            productName = productWishlist.productName,
            productVariant = productWishlist.chipVariant,
            productStock = productWishlist.stock,
            productPrice = productWishlist.productPrice,
            brand = productWishlist.brand
        )
        viewModelScope.launch { cartRepository.insertToCart(productCart) }
    }

    fun deleteAllFromWishlist() {
        viewModelScope.launch { wishlistRepository.deleteAllWishlist() }
    }

    init {
        getAllWishlist()
    }
}