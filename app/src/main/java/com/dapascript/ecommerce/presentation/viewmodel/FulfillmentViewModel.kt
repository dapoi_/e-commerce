package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.cart.fulfillment.FulfillmentRepository
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponse
import com.dapascript.ecommerce.data.source.remote.model.RatingResponse
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FulfillmentViewModel @Inject constructor(
    private val fulfillmentRepository: FulfillmentRepository
) : ViewModel() {

    private var _postFulfillment = MutableLiveData<Resource<FulfillmentResponse>>()
    val postFulfillment: LiveData<Resource<FulfillmentResponse>> get() = _postFulfillment

    private var _ratingProduct = MutableLiveData<Resource<RatingResponse>>()
    val ratingProduct: LiveData<Resource<RatingResponse>> get() = _ratingProduct

    fun postFullfillment(
        payment: String,
        productId: String,
        variantName: String,
        quantity: Int
    ) {
        viewModelScope.launch {
            fulfillmentRepository.postFullfillment(
                payment, productId, variantName, quantity
            ).collect { _postFulfillment.value = it }
        }
    }

    fun rateProduct(
        invoiceId: String,
        rating: Int?,
        review: String?
    ) {
        viewModelScope.launch {
            fulfillmentRepository.rateProduct(
                invoiceId, rating, review
            ).collect { _ratingProduct.value = it }
        }
    }
}