package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.auth.AuthRepository
import com.dapascript.ecommerce.data.source.remote.model.LoginResponse
import com.dapascript.ecommerce.data.source.remote.model.RegisterResponse
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val analyticsManager: AnalyticsManager
) : ViewModel() {

    private var _registerState = MutableLiveData<Resource<RegisterResponse>>()
    val registerState: LiveData<Resource<RegisterResponse>> get() = _registerState

    private var _loginState = MutableLiveData<Resource<LoginResponse>>()
    val loginState: LiveData<Resource<LoginResponse>> get() = _loginState

    fun signUpAccount(email: String, password: String) {
        viewModelScope.launch {
            authRepository.registerAccount(email, password).collect { result ->
                _registerState.postValue(result)
            }
        }

        analyticsManager.logSignUpEvent("email")
    }

    fun signInAccount(email: String, password: String) {
        viewModelScope.launch {
            authRepository.loginAccount(email, password).collect { result ->
                _loginState.postValue(result)
            }
        }

        analyticsManager.logSignInEvent("email")
    }
}