package com.dapascript.ecommerce.presentation.view.main.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.databinding.FragmentHomeBinding
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.CartViewModel
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.dapascript.ecommerce.presentation.viewmodel.NotifViewModel
import com.dapascript.ecommerce.presentation.viewmodel.WishlistViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(
    FragmentHomeBinding::inflate
) {

    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()
    private val notifViewModel: NotifViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        dataStoreViewModel.getUserName.observe(viewLifecycleOwner) { userName ->
            if (userName.isNullOrEmpty()) {
                try {
                    findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToProfileFragment())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        binding.btnLogout.setOnClickListener {
            cartViewModel.deleteAllProduct()
            wishlistViewModel.deleteAllFromWishlist()
            notifViewModel.deleteAllNotif()
            dataStoreViewModel.signOut()
        }
    }
}