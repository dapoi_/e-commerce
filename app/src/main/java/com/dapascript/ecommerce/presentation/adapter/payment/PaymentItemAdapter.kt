package com.dapascript.ecommerce.presentation.adapter.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.data.source.remote.model.PaymentItem
import com.dapascript.ecommerce.databinding.ItemListPaymentBinding

class PaymentItemAdapter(
    private var onItemClick: ((PaymentItem) -> Unit)? = null
) : RecyclerView.Adapter<PaymentItemAdapter.PaymentItemViewHolder>() {

    private var getListPaymentItem = ArrayList<PaymentItem>()

    fun setListPaymentItem(listPaymentItem: List<PaymentItem>) {
        getListPaymentItem.clear()
        getListPaymentItem.addAll(listPaymentItem)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentItemViewHolder {
        return PaymentItemViewHolder(
            ItemListPaymentBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = getListPaymentItem.size

    override fun onBindViewHolder(holder: PaymentItemViewHolder, position: Int) {
        holder.bind(getListPaymentItem[position])
    }

    inner class PaymentItemViewHolder(
        private val binding: ItemListPaymentBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(paymentItem: PaymentItem) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(paymentItem.image)
                    .into(ivPayment)
                tvPaymentName.text = paymentItem.label
                divider.isVisible = absoluteAdapterPosition != getListPaymentItem.size - 1
                if (paymentItem.status) {
                    root.alpha = 1f
                } else {
                    root.alpha = 0.5f
                    root.background = null
                }
            }

            itemView.setOnClickListener {
                onItemClick?.invoke(getListPaymentItem[absoluteAdapterPosition])
            }
        }
    }
}