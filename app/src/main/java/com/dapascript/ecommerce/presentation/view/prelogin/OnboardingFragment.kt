package com.dapascript.ecommerce.presentation.view.prelogin

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentOnboardingBinding
import com.dapascript.ecommerce.presentation.adapter.onboarding.OnboardingAdapter
import com.dapascript.ecommerce.presentation.adapter.onboarding.OnboardingItem
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnboardingFragment : BaseFragment<FragmentOnboardingBinding>(
    FragmentOnboardingBinding::inflate
) {

    private lateinit var onboardingAdapter: OnboardingAdapter

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    private val dataStoreViewModel: DataStoreViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val data = listOf(
            OnboardingItem(
                R.drawable.ic_onboard_1
            ),
            OnboardingItem(
                R.drawable.ic_onboard_2
            ),
            OnboardingItem(
                R.drawable.ic_onboard_3
            )
        )
        onboardingAdapter = OnboardingAdapter()
        onboardingAdapter.setList(data)

        binding.apply {
            vpOnboard.adapter = onboardingAdapter
            vpOnboard.offscreenPageLimit = 1
            val checkStateNext = object : OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    btnNext.visibility =
                        if (position == onboardingAdapter.itemCount.minus(1)) View.GONE
                        else View.VISIBLE
                }
            }

            vpOnboard.registerOnPageChangeCallback(checkStateNext)

            TabLayoutMediator(tabIndicator, vpOnboard) { tab: TabLayout.Tab, _: Int ->
                vpOnboard.setCurrentItem(tab.position, true)
            }.attach()

            btnSignUp.setOnClickListener {
                vpOnboard.unregisterOnPageChangeCallback(checkStateNext)
                dataStoreViewModel.saveOnboardingState(true)
                findNavController().navigate(OnboardingFragmentDirections.actionOnboardingFragmentToSignUpFragment())
                analyticsManager.buttonClickEvent(btnSignUp.text.toString())
            }

            btnNext.setOnClickListener {
                vpOnboard.setCurrentItem(getItem() + 1, true)
            }

            btnSkip.setOnClickListener {
                vpOnboard.unregisterOnPageChangeCallback(checkStateNext)
                dataStoreViewModel.saveOnboardingState(true)
                findNavController().navigate(OnboardingFragmentDirections.actionOnboardingFragmentToSignInFragment())
            }
        }
    }

    private fun getItem(): Int = binding.vpOnboard.currentItem
}