package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.detail.review.ReviewRepository
import com.dapascript.ecommerce.data.source.remote.model.ReviewsItem
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val reviewRepository: ReviewRepository
) : ViewModel() {

    private var _getReviewsResult = MutableLiveData<Resource<List<ReviewsItem>>>()
    val getReviewsResult: LiveData<Resource<List<ReviewsItem>>> get() = _getReviewsResult

    fun getReviews(productId: String) {
        viewModelScope.launch {
            reviewRepository.getReviews(productId).collect {
                _getReviewsResult.postValue(it)
            }
        }
    }
}