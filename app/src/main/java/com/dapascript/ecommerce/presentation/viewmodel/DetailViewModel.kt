package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.data.repository.detail.DetailRepository
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    private val detailRepository: DetailRepository,
    private val cartRepository: CartRepository,
    private val analyticsManager: AnalyticsManager
) : ViewModel() {

    private var _getDetailResult = MutableLiveData<Resource<DetailItem>>()
    val getDetailResult: LiveData<Resource<DetailItem>> get() = _getDetailResult

    fun getDetail(id: String) {
        viewModelScope.launch {
            detailRepository.getProduct(id).collect {
                _getDetailResult.value = it
            }
        }
    }

    fun insertToCart(
        detailItem: DetailItem
    ) {
        val product = toCart(detailItem)
        viewModelScope.launch { cartRepository.insertToCart(product) }
        analyticsManager.addToCartEvent(detailItem)
    }

    fun insertToWishlist() {
        val product = toWishlist((getDetailResult.value as Resource.Success).data)
        viewModelScope.launch { detailRepository.insertToWishlist(product) }
        analyticsManager.addToWishlistEvent(product)
    }

    fun deleteFromWishlist() {
        val product = toWishlist((getDetailResult.value as Resource.Success).data)
        viewModelScope.launch { detailRepository.deleteWishlistFromDetail(product) }
    }

    fun checkProductWishlist(id: String): LiveData<Boolean> =
        detailRepository.checkProductWishlist(id).distinctUntilChanged().asLiveData()

    private fun toCart(product: DetailItem): ProductCartEntity {
        product.let {
            return ProductCartEntity(
                it.productId,
                it.image[0],
                it.productName,
                it.productVariant[0].variantName,
                it.stock.toString(),
                it.productPrice.toString(),
                it.brand
            )
        }
    }

    private fun toWishlist(product: DetailItem): ProductWishlistEntity {
        product.let {
            return ProductWishlistEntity(
                it.productId,
                it.image[0],
                it.productName,
                it.productPrice.toString(),
                it.store,
                it.productRating.toString(),
                it.sale,
                it.productVariant[0].variantName,
                it.stock.toString(),
                it.brand
            )
        }
    }
}