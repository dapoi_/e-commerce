package com.dapascript.ecommerce.presentation.adapter.notif

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import com.dapascript.ecommerce.databinding.ItemListNotifBinding

class NotifAdapter(
    private val readNotif: ((NotifEntity) -> Unit)? = null
) : RecyclerView.Adapter<NotifAdapter.NotifViewHolder>() {

    private var getListNotif = ArrayList<NotifEntity>()

    fun setListNotif(listNotif: List<NotifEntity>) {
        getListNotif.clear()
        getListNotif.addAll(listNotif)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotifViewHolder {
        return NotifViewHolder(
            ItemListNotifBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = getListNotif.size

    override fun onBindViewHolder(holder: NotifViewHolder, position: Int) {
        holder.bind(getListNotif[position])
    }

    inner class NotifViewHolder(
        private val binding: ItemListNotifBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: NotifEntity) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(data.image)
                    .into(ivNotif)

                tvInfoNotif.text = data.type
                tvTitleNotif.text = data.title
                tvDescNotif.text = data.desc
                tvDateTimeNotif.text = "${data.date}, ${data.time}"

                root.background = if (data.isRead) {
                    ContextCompat.getDrawable(itemView.context, R.color.white)
                } else {
                    ContextCompat.getDrawable(itemView.context, R.color.purple_200)
                }
            }
        }

        init {
            binding.root.setOnClickListener {
                readNotif?.invoke(getListNotif[absoluteAdapterPosition])
            }
        }
    }
}