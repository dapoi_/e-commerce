package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.main.store.StoreRepository
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
    private val analyticsManager: AnalyticsManager
) : ViewModel() {

    private var _getListProduct = MutableStateFlow(FilterProduct())
    @OptIn(ExperimentalCoroutinesApi::class)
    val getListProducts: Flow<PagingData<ProductEntity>> = _getListProduct.flatMapLatest {
        storeRepository.getProducts(
            search = it.query,
            brand = it.chipCategory,
            lowest = it.minPrice?.toIntOrNull(),
            highest = it.maxPrice?.toIntOrNull(),
            sort = it.chipSort,
            limit = 10,
            page = 1
        )
    }.cachedIn(viewModelScope)

    private var _filterProduct = MutableLiveData<FilterProduct>()
    val filterProduct: LiveData<FilterProduct> get() = _filterProduct

    private var _isLinearLayout = MutableLiveData(true)
    val isLinearLayout: LiveData<Boolean> get() = _isLinearLayout

    fun setListProduct(filter: FilterProduct) {
        if (filter == _getListProduct.value) {
            return
        }

        _getListProduct.update {
            filter
        }
    }

    fun setStateChip(filterProduct: FilterProduct) {
        _filterProduct.value = filterProduct
        analyticsManager.selectItemFilterEvent(filterProduct)
    }

    fun isLinearLayout() {
        _isLinearLayout.value = !_isLinearLayout.value!!
    }

    data class FilterProduct(
        var chipSort: String? = null,
        var chipCategory: String? = null,
        var minPrice: String? = null,
        var maxPrice: String? = null,
        var query: String? = null,
    )
}