package com.dapascript.ecommerce.presentation.view.notif

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.databinding.FragmentNotifBinding
import com.dapascript.ecommerce.presentation.adapter.notif.NotifAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.NotifViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotifFragment : BaseFragment<FragmentNotifBinding>(FragmentNotifBinding::inflate) {

    private val notifViewModel: NotifViewModel by viewModels()
    private lateinit var notifAdapter: NotifAdapter

    private lateinit var id: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getString("id", "")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            toolbar.setNavigationOnClickListener { findNavController().popBackStack() }

            notifAdapter = NotifAdapter(
                readNotif = {
                    notifViewModel.setReadNotif(it)
                }
            )
            rvNotif.adapter = notifAdapter

            notifViewModel.getAllNotif.observe(viewLifecycleOwner) { response ->
                layoutError.root.isVisible = response.isNullOrEmpty()
                layoutError.btnError.isVisible = false
                rvNotif.isVisible = !response.isNullOrEmpty()
                notifAdapter.setListNotif(response)
            }
        }
    }
}