package com.dapascript.ecommerce.presentation.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.dapascript.ecommerce.MainDirections
import com.dapascript.ecommerce.MainNavDirections
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.databinding.ActivityMainBinding
import com.dapascript.ecommerce.presentation.viewmodel.CartViewModel
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.dapascript.ecommerce.presentation.viewmodel.NotifViewModel
import com.dapascript.ecommerce.presentation.viewmodel.WishlistViewModel
import com.dapascript.ecommerce.utils.capitalizeEachWord
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navHostFragment: NavHostFragment
    private lateinit var navController: NavController

    @Inject
    lateinit var firebaseMessaging: FirebaseMessaging

    @Inject
    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val notifViewModel: NotifViewModel by viewModels()
    private var keep = true
    private val delay = 1500L

    private val launchPermissionNotification = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().setKeepOnScreenCondition { keep }
        Handler(Looper.getMainLooper()).postDelayed({
            keep = false
        }, delay)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        setUpNavHost()
        checkStateUserLogin()
        toolbarFeature()
        stateHideShowBottomNav()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) checkNotificationPermission()

        val notifData = intent.data
        if (notifData != null) {
            val id = notifData.getQueryParameter("id")
            if (id != null) {
                navController.navigate("ecommerce://notif/$id")
            }
        }
    }

    private fun setUpNavHost() {
        navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        binding.bottomNav.apply {
            setupWithNavController(navController)
            wishlistViewModel.wishlist.observe(this@MainActivity) { response ->
                if (response.isNotEmpty()) {
                    getOrCreateBadge(R.id.wishlistFragment).number = response.size
                } else {
                    removeBadge(R.id.wishlistFragment)
                }
            }
        }
    }

    private fun checkStateUserLogin() {
        dataStoreViewModel.apply {
            getAccessToken.observe(this@MainActivity) { accessToken ->
                if (accessToken.isEmpty()) {
                    navController.navigate(MainDirections.actionGlobalAuth())
                } else {
                    firebaseSetUp()
                }
            }

            getUserName.observe(this@MainActivity) { userName ->
                if (userName.isNotEmpty()) {
                    binding.tvUsername.text = capitalizeEachWord(userName)
                }
            }
        }
    }

    private fun firebaseSetUp() {
        firebaseMessaging.subscribeToTopic("promo")
        firebaseRemoteConfig.fetchAndActivate()
    }

    private fun toolbarFeature() {
        binding.apply {
            ivNotif.setOnClickListener {
                navController.navigate(MainNavDirections.actionGlobalNotifFragment())
            }
            ivCart.setOnClickListener {
                navController.navigate(MainNavDirections.actionGlobalCartFragment())
            }

            cartViewModel.getCartProduct().observe(this@MainActivity) { cartProduct ->
                if (cartProduct.isNotEmpty()) {
                    tvCartBadge.visibility = View.VISIBLE
                    tvCartBadge.text = cartProduct.size.toString()
                } else {
                    tvCartBadge.visibility = View.GONE
                }
            }

            notifViewModel.getAllNotif.observe(this@MainActivity) { response ->
                response.filter { !it.isRead }.let { unreadNotif ->
                    if (unreadNotif.isEmpty()) {
                        tvNotifBadge.visibility = View.GONE
                    } else {
                        tvNotifBadge.visibility = View.VISIBLE
                        tvNotifBadge.text = unreadNotif.size.toString()
                    }
                }
            }
        }
    }

    private fun stateHideShowBottomNav() {
        val listHideBottomNavFragment = listOf(
            R.id.onboardingFragment,
            R.id.signInFragment,
            R.id.signUpFragment,
            R.id.profileFragment,
            R.id.cameraFragment,
            R.id.searchFragment,
            R.id.detailProductFragment,
            R.id.userReviewFragment,
            R.id.cartFragment,
            R.id.checkoutFragment,
            R.id.paymentFragment,
            R.id.statusPaymentFragment,
            R.id.notifFragment
        )

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id in listHideBottomNavFragment) {
                showBottomNavAndToolbar(false)
            } else {
                showBottomNavAndToolbar(true)
            }
        }
    }

    private fun showBottomNavAndToolbar(state: Boolean) {
        binding.apply {
            if (state) {
                bottomNav.visibility = View.VISIBLE
                toolbar.visibility = View.VISIBLE
                vDividerBottom.visibility = View.VISIBLE
                vDividerTop.visibility = View.VISIBLE
            } else {
                bottomNav.visibility = View.GONE
                toolbar.visibility = View.GONE
                vDividerBottom.visibility = View.GONE
                vDividerTop.visibility = View.GONE
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun checkNotificationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            launchPermissionNotification.launch(Manifest.permission.POST_NOTIFICATIONS)
        }
    }

    fun customSnackbar(message: String, state: Boolean) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).apply {
            setBackgroundTint(
                ContextCompat.getColor(
                    this@MainActivity,
                    if (state) R.color.red else R.color.purple_500
                )
            )
            anchorView = binding.bottomNav
            show()
        }
    }
}