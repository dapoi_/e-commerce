package com.dapascript.ecommerce.presentation.adapter.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.databinding.ItemOnboardingBinding

class OnboardingAdapter : RecyclerView.Adapter<OnboardingAdapter.OnboardingViewHolder>() {

    private var listItem = ArrayList<OnboardingItem>()

    fun setList(list: List<OnboardingItem>) {
        listItem.clear()
        listItem.addAll(list)
        notifyDataSetChanged()
    }

    inner class OnboardingViewHolder(
        private val binding: ItemOnboardingBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: OnboardingItem) {
            Glide.with(itemView.context)
                .load(data.image)
                .into(binding.ivOnboarding)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OnboardingViewHolder {
        val binding =
            ItemOnboardingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OnboardingViewHolder(binding)
    }

    override fun getItemCount(): Int = listItem.size

    override fun onBindViewHolder(holder: OnboardingViewHolder, position: Int) {
        holder.bind(listItem[position])
    }
}

data class OnboardingItem(
    val image: Int
)