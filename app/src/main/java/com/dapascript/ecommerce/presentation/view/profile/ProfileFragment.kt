package com.dapascript.ecommerce.presentation.view.profile

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentProfileBinding
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.dapascript.ecommerce.presentation.viewmodel.ProfileViewModel
import com.dapascript.ecommerce.utils.changedSpecificTermsConditionColor
import com.dapascript.ecommerce.utils.hideKeyboard
import com.dapascript.ecommerce.utils.rotateBitmap
import com.dapascript.ecommerce.utils.uriToFile
import com.dapascript.ecommerce.vo.Resource
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>(
    FragmentProfileBinding::inflate
) {

    private lateinit var result: Bitmap

    private val profileViewModel: ProfileViewModel by viewModels()
    private val dataStoreViewModel: DataStoreViewModel by viewModels()

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    private var getFile: File? = null

    private val launchGallery = registerForActivityResult(
        ActivityResultContracts.PickVisualMedia()
    ) { uri ->
        val file = uri?.let { uriToFile(it, requireActivity()) }
        getFile = file

        binding.apply {
            ivDefaultPhotoProfile.visibility = if (file != null) View.GONE else View.VISIBLE
            Glide.with(requireContext())
                .load(uri)
                .placeholder(R.color.purple_500)
                .into(ivPhotoProfile)
        }
    }

    private val launchCamera = registerForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) { isGranted ->
        if (isGranted) {
            findNavController().navigate(
                ProfileFragmentDirections.actionProfileFragmentToCameraFragment()
            )
        } else {
            binding.let {
                Snackbar.make(
                    it.root,
                    "Perizinan kamera dibutuhkan untuk mengambil gambar",
                    Snackbar.LENGTH_LONG
                ).apply {
                    setAction("Buka Pengaturan") {
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", requireActivity().packageName, null)
                        intent.data = uri
                        startActivity(intent)
                    }
                    show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(CAMERA_X_RESULT) { _, bundle ->
            val image = bundle.getSerializable(PICTURE) as File
            val isBackCamera = bundle.getBoolean(IS_BACK_CAMERA, false)
            getFile = image

            result = rotateBitmap(
                BitmapFactory.decodeFile(image.absolutePath),
                isBackCamera
            )

            binding.apply {
                ivDefaultPhotoProfile.visibility = View.GONE
                ivPhotoProfile.setImageBitmap(result)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            changedSpecificTermsConditionColor(tvTermsCondition, requireActivity())

            ivPhotoProfile.setOnClickListener {
                showDialog()
            }

            etProfileName.doOnTextChanged { text, _, _, _ ->
                btnSave.isEnabled = text.toString().isNotEmpty()
            }

            btnSave.setOnClickListener {
                hideKeyboard(requireActivity())
                val userName = etProfileName.text.toString().toRequestBody(
                    "text/plain".toMediaType()
                )
                val reqBodyImage = getFile?.asRequestBody("image/*".toMediaTypeOrNull())
                val userImage = reqBodyImage?.let { multiPartImage ->
                    MultipartBody.Part.createFormData(
                        "userImage",
                        getFile?.name,
                        multiPartImage
                    )
                }

                dataStoreViewModel.saveUsername(etProfileName.text.toString())
                profileViewModel.createProfile(userName, userImage ?: null)

                analyticsManager.buttonClickEvent("Save Profile")
            }

            profileViewModel.makeProfile.observe(viewLifecycleOwner) { result ->
                progressBar.isVisible = result is Resource.Loading
                if (result is Resource.Success) {
                    dataStoreViewModel.saveUsername(result.data.data.userName)
                    findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToHomeFragment())
                } else if (result is Resource.Error) {
                    Toast.makeText(
                        requireContext(),
                        "Profile Fragment: $result",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun showDialog() {
        val itemsChoosePhoto = arrayOf("Kamera", "Galeri")
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Pilih Gambar")
            .setItems(itemsChoosePhoto) { _, which ->
                when (which) {
                    0 -> openCamera()
                    1 -> openGallery()
                }
            }
            .show()
    }

    private fun openCamera() {
        val cameraPermission = Manifest.permission.CAMERA
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                cameraPermission
            ) == PackageManager.PERMISSION_GRANTED -> {
                findNavController().navigate(
                    ProfileFragmentDirections.actionProfileFragmentToCameraFragment()
                )
            }

            shouldShowRequestPermissionRationale(cameraPermission) -> {
                binding.let {
                    Snackbar.make(
                        it.root,
                        "Perizinan kamera dibutuhkan untuk mengambil gambar",
                        Snackbar.LENGTH_LONG
                    ).apply {
                        setAction("Buka Pengaturan") {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri = Uri.fromParts("package", requireActivity().packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        }
                        show()
                    }
                }
            }

            else -> {
                launchCamera.launch(cameraPermission)
            }
        }
    }

    private fun openGallery() {
        launchGallery.launch(PickVisualMediaRequest(ActivityResultContracts.PickVisualMedia.ImageOnly))
    }

    companion object {
        const val CAMERA_X_RESULT = "100"
        const val PICTURE = "picture"
        const val IS_BACK_CAMERA = "isBackCamera"
    }
}