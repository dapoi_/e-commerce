package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.preference.DataStorePreference
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DataStoreViewModel @Inject constructor(
    private val dataStorePreference: DataStorePreference
) : ViewModel() {

    /**
     * save onboarding state
     */
    fun saveOnboardingState(state: Boolean) {
        viewModelScope.launch {
            dataStorePreference.saveOnboardingState(state)
        }
    }

    /**
     * save token
     */
    fun saveAccessToken(token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            dataStorePreference.saveAccessToken(token)
        }
    }

    /**
     * save refresh token
     */
    fun saveRefreshToken(token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            dataStorePreference.saveRefreshToken(token)
        }
    }

    /**
     * save username
     */
    fun saveUsername(username: String) {
        viewModelScope.launch {
            dataStorePreference.saveUserName(username)
        }
    }

    /**
     * get token
     */
    val getAccessToken = dataStorePreference.getAccessToken

    /**
     * get onboarding state
     */
    val getOnboardingState = dataStorePreference.getOnboardingState

    /**
     * get username
     */
    val getUserName = dataStorePreference.getUserName

    /**
     * clear token
     */
    fun signOut() {
        viewModelScope.launch {
            dataStorePreference.signOut()
        }
    }
}