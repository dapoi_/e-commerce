package com.dapascript.ecommerce.presentation.adapter.review

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.remote.model.ReviewsItem
import com.dapascript.ecommerce.databinding.ItemListReviewBinding
import com.dapascript.ecommerce.utils.capitalizeEachWord

class ReviewAdapter : RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {

    private var getListReview = ArrayList<ReviewsItem>()

    fun setListReview(listReview: List<ReviewsItem>) {
        getListReview.clear()
        getListReview.addAll(listReview)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        return ReviewViewHolder(
            ItemListReviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = getListReview.size

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.bind(getListReview[position])
    }

    inner class ReviewViewHolder(
        private val binding: ItemListReviewBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(data: ReviewsItem) {
            binding.apply {
                Glide.with(itemView.context)
                    .load(data.userImage)
                    .placeholder(R.drawable.ic_image_default)
                    .into(ivPhoto)
                tvName.text = capitalizeEachWord(data.userName)
                tvReview.text = data.userReview
                rbRating.rating = data.userRating.toFloat()
            }
        }
    }
}