package com.dapascript.ecommerce.presentation.adapter.product

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.ProductWishlistEntity
import com.dapascript.ecommerce.databinding.ItemGridWishlistBinding
import com.dapascript.ecommerce.databinding.ItemListWishlistBinding
import com.dapascript.ecommerce.utils.currencyIndonesia

class WishlistAdapter(
    private var onClickItem: ((ProductWishlistEntity) -> Unit)? = null,
    private var deleteItem: ((ProductWishlistEntity) -> Unit)? = null,
    private var addToCart: ((ProductWishlistEntity) -> Unit)? = null
) : ListAdapter<ProductWishlistEntity, ViewHolder>(DIFF_CALLBACK) {

    private var viewType = 0

    fun setViewType(viewType: Int) {
        this.viewType = viewType
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (viewType) {
            0 -> {
                val binding = ItemListWishlistBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                WishlistViewHolder(binding)
            }

            else -> {
                val binding = ItemGridWishlistBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                WishlistGridViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val wishlist = getItem(position)
        when (holder) {
            is WishlistViewHolder -> holder.bind(wishlist)
            is WishlistGridViewHolder -> holder.bind(wishlist)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    inner class WishlistViewHolder(
        private val binding: ItemListWishlistBinding
    ) : ViewHolder(binding.root) {
        fun bind(wishlist: ProductWishlistEntity) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(wishlist.productImage)
                    .into(ivProduct)
                tvProductName.text = wishlist.productName
                tvProductPrice.text = currencyIndonesia(wishlist.productPrice)
                tvSellerName.text = wishlist.sellerName
                tvRatingSold.text = itemView.context.getString(
                    R.string.rating_sold,
                    wishlist.rate,
                    wishlist.totalSold.toString()
                )
            }
        }

        init {
            binding.apply {
                clDelete.setOnClickListener {
                    deleteItem?.invoke(getItem(absoluteAdapterPosition))
                }

                cardProduct.setOnClickListener {
                    onClickItem?.invoke(getItem(absoluteAdapterPosition))
                }

                btnAddCart.setOnClickListener {
                    addToCart?.invoke(getItem(absoluteAdapterPosition))
                }
            }
        }
    }

    inner class WishlistGridViewHolder(
        private val binding: ItemGridWishlistBinding
    ) : ViewHolder(binding.root) {
        fun bind(wishlist: ProductWishlistEntity) {
            with(binding) {
                Glide.with(itemView.context)
                    .load(wishlist.productImage)
                    .into(ivProduct)
                tvProductName.text = wishlist.productName
                tvProductPrice.text = currencyIndonesia(wishlist.productPrice)
                tvSellerName.text = wishlist.sellerName
                tvRatingSold.text = itemView.context.getString(
                    R.string.rating_sold,
                    wishlist.rate,
                    wishlist.totalSold.toString()
                )
            }
        }

        init {
            binding.apply {
                clDelete.setOnClickListener {
                    deleteItem?.invoke(getItem(absoluteAdapterPosition))
                }

                cardProduct.setOnClickListener {
                    onClickItem?.invoke(getItem(absoluteAdapterPosition))
                }

                btnAddCart.setOnClickListener {
                    addToCart?.invoke(getItem(absoluteAdapterPosition))
                }
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ProductWishlistEntity>() {
            override fun areItemsTheSame(
                oldItem: ProductWishlistEntity,
                newItem: ProductWishlistEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ProductWishlistEntity,
                newItem: ProductWishlistEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}