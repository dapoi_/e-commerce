package com.dapascript.ecommerce.presentation.view.main.store.bottomsheet

import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.databinding.FragmentBottomSheetBinding
import com.dapascript.ecommerce.utils.applyEditTextCurrencyIndonesia
import com.dapascript.ecommerce.utils.convertCurrencyToNumber
import com.dapascript.ecommerce.utils.currencyIndonesia
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.android.material.textfield.TextInputEditText

class BottomSheetFragment(private val callback: PassChip) : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentBottomSheetBinding
    private lateinit var chip: Chip

    private var chipSelected: Map<String, String> = emptyMap()

    interface PassChip {
        fun selectedData(chip: Map<String, String>?, minPrice: String?, maxPrice: String?)
        fun reset()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        val contextWrapper = ContextThemeWrapper(requireContext(), R.style.Theme_ECommerce)
        binding = FragmentBottomSheetBinding.inflate(inflater.cloneInContext(contextWrapper))
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val behavior = (dialog as BottomSheetDialog).behavior
        dialog?.setOnShowListener {
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        val chipSort = arguments?.getStringArray("chipSort")?.toList() ?: emptyList()
        val chipCategory = arguments?.getStringArray("chipCategory")?.toList() ?: emptyList()
        val filterChipData = arguments?.getStringArray("filterChipData")?.toList() ?: emptyList()
        val minPriceData = arguments?.getString("minPrice") ?: ""
        val maxPriceData = arguments?.getString("maxPrice") ?: ""

        addChipsToChipGroup("Urutkan", chipSort, filterChipData, binding.cgSort)
        addChipsToChipGroup("Kategori", chipCategory, filterChipData, binding.cgCategory)

        binding.apply {
            setEditTextStatus(minPriceData, maxPriceData)
            applyEditTextCurrencyIndonesia(etLowestPrice)
            applyEditTextCurrencyIndonesia(etHighestPrice)

            btnShowProduct.setOnClickListener {
                callback.selectedData(
                    chipSelected,
                    convertCurrencyToNumber(etLowestPrice.text.toString()),
                    convertCurrencyToNumber(etHighestPrice.text.toString())
                )
                dismiss()
            }

            tvReset.isVisible =
                filterChipData.isNotEmpty() || minPriceData.isNotEmpty() || maxPriceData.isNotEmpty()
            tvReset.setOnClickListener {
                tvReset.isVisible = false
                chipSelected = emptyMap()
                cgSort.clearCheck()
                cgCategory.clearCheck()
                etLowestPrice.setText("")
                etHighestPrice.setText("")
                etLowestPrice.clearFocusAndHideCursor()
                etHighestPrice.clearFocusAndHideCursor()
            }
        }
    }

    private fun setEditTextStatus(minPriceData: String, maxPriceData: String) {
        binding.apply {
            etLowestPrice.apply {
                clearFocusAndHideCursor()
                setText(if (minPriceData.isEmpty()) "" else currencyIndonesia(minPriceData))
            }
            etHighestPrice.apply {
                clearFocusAndHideCursor()
                setText(if (maxPriceData.isEmpty()) "" else currencyIndonesia(maxPriceData))
            }
        }
    }

    private fun TextInputEditText.clearFocusAndHideCursor() {
        clearFocus()
        isCursorVisible = false
    }

    private fun addChipsToChipGroup(
        kindOfChip: String,
        allChipData: List<String>,
        filterChipData: List<String>,
        chipGroup: ViewGroup
    ) {
        allChipData.forEach { chipData ->
            chip = Chip(requireContext()).apply {
                text = chipData
                isCheckable = true
                setOnCheckedChangeListener { _, isChecked ->
                    chipSelected = if (isChecked) {
                        chipSelected.plus(kindOfChip to chipData)
                    } else {
                        chipSelected.minus(kindOfChip)
                    }
                }
            }
            chipGroup.addView(chip)

            if (filterChipData.contains(chipData)) {
                chip.isChecked = true
                chipSelected = chipSelected.plus(kindOfChip to chipData)
            }
        }
    }
}