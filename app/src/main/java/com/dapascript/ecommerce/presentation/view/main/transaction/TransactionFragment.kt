package com.dapascript.ecommerce.presentation.view.main.transaction

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.source.remote.model.FulfillmentResponseItem
import com.dapascript.ecommerce.databinding.FragmentTransactionBinding
import com.dapascript.ecommerce.presentation.adapter.transaction.TransactionAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.TransactionViewModel
import com.dapascript.ecommerce.utils.handleError
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TransactionFragment : BaseFragment<FragmentTransactionBinding>(
    FragmentTransactionBinding::inflate
) {

    private val transactionViewModel: TransactionViewModel by viewModels()

    private lateinit var transactionAdapter: TransactionAdapter

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setAdapter()
        setViewModel()
    }

    private fun setAdapter() {
        binding.apply {
            transactionAdapter = TransactionAdapter(
                onReviewClicked = {
                    val fulfillmentResponseItem = FulfillmentResponseItem(
                        date = it.date,
                        total = it.total,
                        invoiceId = it.invoiceId,
                        payment = it.payment,
                        time = it.time,
                        status = it.status,
                    )
                    findNavController().navigate(
                        TransactionFragmentDirections.actionTransactionFragmentToStatusPaymentFragment(
                            fulfillmentResponseItem, true
                        )
                    )

                    analyticsManager.buttonClickEvent("Review Product in Transaction")
                }
            )
            rvTransaction.adapter = transactionAdapter
        }
    }

    private fun setViewModel() {
        binding.apply {
            transactionViewModel.getTransactionResult().observe(viewLifecycleOwner) { response ->
                progressBar.isVisible = response is Resource.Loading
                rvTransaction.isVisible = response is Resource.Success
                layoutError.btnError.visibility = View.GONE

                if (response is Resource.Success) {
                    if (response.data.isEmpty()) {
                        layoutError.root.isVisible = true
                    } else {
                        transactionAdapter.submitList(response.data)
                    }
                } else if (response is Resource.Error) {
                    layoutError.root.isVisible = true
                    handleError(
                        layoutError,
                        response.throwable,
                        requireContext()
                    )
                }
            }
        }
    }
}