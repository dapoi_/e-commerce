package com.dapascript.ecommerce.presentation.view.main.store.search

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.dapascript.ecommerce.databinding.FragmentSearchBinding
import com.dapascript.ecommerce.presentation.adapter.product.SearchListAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.main.store.StoreFragment
import com.dapascript.ecommerce.presentation.viewmodel.SearchViewModel
import com.dapascript.ecommerce.utils.handleError
import com.dapascript.ecommerce.utils.hideKeyboard
import com.dapascript.ecommerce.utils.showKeyboard
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(
    FragmentSearchBinding::inflate
) {

    private val searchListAdapter: SearchListAdapter by lazy { SearchListAdapter() }
    private val searchViewModel: SearchViewModel by viewModels()
    private val args: SearchFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {

            requireActivity().onBackPressedDispatcher.addCallback(
                viewLifecycleOwner,
                object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        if (etSearch.text.isNullOrEmpty()) {
                            sendDataToStoreFragment(null)
                        } else {
                            sendDataToStoreFragment(etSearch.text.toString())
                        }
                    }
                }
            )

            rvSearch.apply {
                adapter = searchListAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }

            searchListAdapter.onClick = {
                sendDataToStoreFragment(it)
            }

            tilSearch.setEndIconOnClickListener {
                etSearch.text?.clear()
                etSearch.clearFocus()
                etSearch.isCursorVisible = false
                layoutError.root.isVisible = false
                progressBar.isVisible = false
            }

            etSearch.apply {

                if (args.search.isNotEmpty()) {
                    setText(args.search)
                    searchViewModel.searchProduct(args.search)
                }

                doOnTextChanged { text, _, _, _ ->
                    searchViewModel.searchProduct(text.toString())
                    if (text.isNullOrEmpty()) searchListAdapter.submitList(null)
                }

                setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        sendDataToStoreFragment(etSearch.text.toString())
                        true
                    } else {
                        false
                    }
                }
            }

            searchViewModel.searchResponse.observe(viewLifecycleOwner) { result ->
                progressBar.isVisible = result is Resource.Loading
                rvSearch.isVisible = result is Resource.Success

                if (result is Resource.Success) {
                    searchListAdapter.submitList(result.data)
                } else if (result is Resource.Error) {
                    handleError(layoutError, result.throwable, requireContext())
                }
            }

            layoutError.btnError.setOnClickListener {
                layoutError.root.isVisible = false
                searchViewModel.searchProduct(etSearch.text.toString())
            }
        }
    }

    private fun sendDataToStoreFragment(query: String?) {
        requireActivity().supportFragmentManager.setFragmentResult(
            StoreFragment.SEARCH_KEY,
            Bundle().apply {
                putString(StoreFragment.SEARCH_RESULT, query)
            }
        )
        hideKeyboard(requireActivity())
        findNavController().popBackStack()
    }

    override fun onResume() {
        super.onResume()
        showKeyboard(requireActivity(), binding.etSearch)
    }
}