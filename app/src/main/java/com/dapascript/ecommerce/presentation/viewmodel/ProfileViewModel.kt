package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.profile.ProfileRepository
import com.dapascript.ecommerce.data.source.remote.model.ProfileResponse
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val profileRepository: ProfileRepository
) : ViewModel() {

    private var _makeProfile = MutableLiveData<Resource<ProfileResponse>>()
    val makeProfile: LiveData<Resource<ProfileResponse>> get() = _makeProfile

    fun createProfile(
        userName: RequestBody,
        userImage: MultipartBody.Part?,
    ) {
        viewModelScope.launch {
            profileRepository.createProfile(userName, userImage).collect { result ->
                _makeProfile.postValue(result)
            }
        }
    }
}