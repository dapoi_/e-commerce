package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.repository.main.store.search.SearchRepository
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val searchRepository: SearchRepository,
    private val analyticsManager: AnalyticsManager
) : ViewModel() {

    private var _searchResponse = MutableLiveData<Resource<List<String>?>>()
    val searchResponse: LiveData<Resource<List<String>?>> get() = _searchResponse

    private var _storeSearchJob: Job? = null

    fun searchProduct(query: String) {
        _storeSearchJob?.cancel()
        if (query.isBlank()) return
        _storeSearchJob = viewModelScope.launch {
            delay(1000)
            searchRepository.searchProduct(query).collect { result ->
                _searchResponse.value = result
            }

            analyticsManager.logSearchResultEvent(query)
        }
    }
}