package com.dapascript.ecommerce.presentation.view.cart.payment

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentStatusPaymentBinding
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.MainActivity
import com.dapascript.ecommerce.presentation.viewmodel.FulfillmentViewModel
import com.dapascript.ecommerce.utils.currencyIndonesia
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StatusPaymentFragment : BaseFragment<FragmentStatusPaymentBinding>(
    FragmentStatusPaymentBinding::inflate
) {

    private val fulfillmentViewModel: FulfillmentViewModel by viewModels()
    private val args: StatusPaymentFragmentArgs by navArgs()
    @Inject
    lateinit var analyticsManager: AnalyticsManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (args.isFromTransaction) {
                        findNavController().popBackStack(R.id.transactionFragment, false)
                    } else {
                        findNavController().popBackStack(R.id.homeFragment, false)
                    }
                }
            }
        )

        binding.apply {

            // args
            val statusPaymentResult = args.fulfillmentResponse
            tvTransactionIdValue.text = statusPaymentResult.invoiceId
            tvTransactionStatusValue.text = statusPaymentResult.status.toString()
            tvTransactionDateValue.text = statusPaymentResult.date
            tvTransactionTimeValue.text = statusPaymentResult.time
            tvPaymentMethodValue.text = statusPaymentResult.payment
            tvTransactionTotalValue.text =
                currencyIndonesia(statusPaymentResult.total.toString())

            btnDone.setOnClickListener {
                fulfillmentViewModel.rateProduct(
                    invoiceId = statusPaymentResult.invoiceId,
                    rating = rbRateProduct.rating.toInt(),
                    review = etRating.text.toString()
                )

                analyticsManager.buttonClickEvent(btnDone.text.toString())
            }

            fulfillmentViewModel.ratingProduct.observe(viewLifecycleOwner) { response ->
                if (response is Resource.Success) {
                    if (args.isFromTransaction) {
                        findNavController().popBackStack(R.id.transactionFragment, false)
                    } else {
                        findNavController().popBackStack(R.id.homeFragment, false)
                    }
                } else if (response is Resource.Error) {
                    (activity as MainActivity).customSnackbar(
                        "Error ${response.throwable.message}",
                        true
                    )
                }
            }
        }
    }
}