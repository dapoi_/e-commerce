package com.dapascript.ecommerce.presentation.view.cart.checkout

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentCheckoutBinding
import com.dapascript.ecommerce.presentation.adapter.product.ProductCheckoutAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.MainActivity
import com.dapascript.ecommerce.presentation.viewmodel.FulfillmentViewModel
import com.dapascript.ecommerce.utils.currencyIndonesia
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutFragment : BaseFragment<FragmentCheckoutBinding>(FragmentCheckoutBinding::inflate) {

    private lateinit var productCheckoutAdapter: ProductCheckoutAdapter

    @Inject lateinit var analyticsManager: AnalyticsManager

    private val fulfillmentViewModel: FulfillmentViewModel by viewModels()
    private val args: CheckoutFragmentArgs by navArgs()
    private var label: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(PAYMENT_REQUEST_KEY) { _, bundle ->
            label = bundle.getString(PAYMENT_LABEL)
            binding.apply {
                Glide.with(requireContext())
                    .load(bundle.getString(PAYMENT_IMAGE))
                    .into(ivCard)
                tvChoosePayment.text = label

                btnPay.isEnabled = true
                analyticsManager.addPaymentInfoEvent(label.toString())
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        setAdapter()
        setViewModel()

        binding.apply {

            toolbar.setNavigationOnClickListener { findNavController().popBackStack() }

            cvChoosePayment.setOnClickListener {
                findNavController().navigate(CheckoutFragmentDirections.actionCheckoutFragmentToPaymentFragment())
            }

            val resultArgs = args.checkoutEntity.toList()

            btnPay.setOnClickListener {
                fulfillmentViewModel.postFullfillment(
                    payment = label.toString(),
                    productId = resultArgs[0].id,
                    variantName = resultArgs[0].productVariant,
                    quantity = resultArgs[0].productQuantity
                )

                analyticsManager.buttonClickEvent(btnPay.text.toString())
            }

            resultArgs.sumOf {
                it.productPrice.toInt() * it.productQuantity
            }.let { currentTotalPrice ->
                tvTotalPrice.text = currencyIndonesia(currentTotalPrice.toString())
            }
        }
    }

    private fun setAdapter() {
        productCheckoutAdapter = ProductCheckoutAdapter(totalPriceChangedListener = { totalPrice ->
            binding.tvTotalPrice.text = currencyIndonesia(totalPrice.toString())
        })
        binding.rvPurchased.apply {
            adapter = productCheckoutAdapter
            itemAnimator = null
        }
        productCheckoutAdapter.submitList(args.checkoutEntity.toList())
    }

    private fun setViewModel() {
        fulfillmentViewModel.postFulfillment.observe(viewLifecycleOwner) { response ->
            binding.apply {
                progressBar.isVisible = response is Resource.Loading
                btnPay.isVisible = response !is Resource.Loading

                if (response is Resource.Success) {
                    val fulfillmentResponse = response.data.data
                    findNavController().navigate(
                        CheckoutFragmentDirections.actionCheckoutFragmentToStatusPaymentFragment(
                            fulfillmentResponse
                        )
                    )

                    analyticsManager.purchaseEvent(response.data.data)
                } else if (response is Resource.Error) {
                    (activity as MainActivity).customSnackbar(
                        "Error: ${response.throwable.message}",
                        true
                    )
                }
            }
        }
    }

    companion object {
        const val PAYMENT_REQUEST_KEY = "paymentRequestKey"
        const val PAYMENT_IMAGE = "image"
        const val PAYMENT_LABEL = "label"
    }
}