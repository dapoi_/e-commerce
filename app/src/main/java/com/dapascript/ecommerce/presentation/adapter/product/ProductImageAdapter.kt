package com.dapascript.ecommerce.presentation.adapter.product

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.databinding.ItemImageProductBinding

class ProductImageAdapter : RecyclerView.Adapter<ProductImageAdapter.ProductImageViewHolder>() {

    private val getImage = ArrayList<String>()

    fun setImage(list: List<String>) {
        getImage.clear()
        getImage.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductImageViewHolder {
        return ProductImageViewHolder(
            ItemImageProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = getImage.size

    override fun onBindViewHolder(holder: ProductImageViewHolder, position: Int) {
        holder.bind(getImage[position])
    }

    inner class ProductImageViewHolder(
        private val binding: ItemImageProductBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
            Glide.with(itemView.context)
                .load(item)
                .placeholder(R.drawable.ic_image_default)
                .into(binding.ivProduct)
        }
    }
}