package com.dapascript.ecommerce.presentation.view.cart

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentCartBinding
import com.dapascript.ecommerce.presentation.adapter.product.CartAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.CartViewModel
import com.dapascript.ecommerce.utils.currencyIndonesia
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : BaseFragment<FragmentCartBinding>(
    FragmentCartBinding::inflate
) {

    private val cartViewModel: CartViewModel by viewModels()

    @Inject
    lateinit var analyticsManager: AnalyticsManager
    private lateinit var cartAdapter: CartAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            toolbar.setNavigationOnClickListener { findNavController().popBackStack() }

            setAdapter()
            setViewModel()

            btnDelete.setOnClickListener {
                if (cbSelectAll.isChecked) {
                    cartViewModel.deleteAllProduct()
                    btnDelete.visibility = View.GONE
                } else {
                    cartAdapter.currentList.forEach { product ->
                        if (product.isSelected) {
                            cartViewModel.deleteProduct(product.id)
                        }
                    }
                }
            }

            btnBuy.setOnClickListener {
                cartViewModel.getSelectedProduct.observe(viewLifecycleOwner) {
                    val action = CartFragmentDirections.actionCartFragmentToCheckoutFragment(
                        checkoutEntity = it.toTypedArray()
                    )
                    findNavController().navigate(action)
                }

                analyticsManager.buttonClickEvent(btnBuy.text.toString())
            }
        }
    }

    private fun setAdapter() {
        binding.rvCart.apply {
            cartAdapter = CartAdapter(
                onCheckEachItem = {
                    cartViewModel.updateCartProduct(
                        it.copy(
                            isSelected = !it.isSelected
                        )
                    )
                },
                onDeleteItem = {
                    cartViewModel.deleteProduct(it.id)
                    analyticsManager.removeFromCartEvent(it)
                },
                onDecreaseQuantity = {
                    cartViewModel.updateCartProduct(
                        it.copy(
                            productQuantity = it.productQuantity - 1
                        )
                    )
                },
                onIncreaseQuantity = {
                    cartViewModel.updateCartProduct(
                        it.copy(
                            productQuantity = it.productQuantity + 1
                        )
                    )
                }
            )
            adapter = cartAdapter
            layoutManager = LinearLayoutManager(requireContext())
            itemAnimator = null
        }
    }

    private fun setViewModel() {
        binding.apply {
            cartViewModel.apply {
                getCartProduct().observe(viewLifecycleOwner) {
                    cartAdapter.submitList(it)

                    cbSelectAll.isVisible = it.isNotEmpty()
                    divider2.isVisible = it.isNotEmpty()
                    divider3.isVisible = it.isNotEmpty()
                    btnBuy.isVisible = it.isNotEmpty()
                    tvTotalPriceTitle.isVisible = it.isNotEmpty()
                    tvTotalPrice.isVisible = it.isNotEmpty()

                    layoutError.root.isVisible = it.isEmpty()
                    layoutError.btnError.visibility = View.GONE

                    analyticsManager.viewCartEvent(it)
                }

                isEachProductSelected.observe(viewLifecycleOwner) {
                    cbSelectAll.isChecked = it
                }

                isAnyProductSelected.observe(viewLifecycleOwner) {
                    btnBuy.isEnabled = it
                    btnDelete.isVisible = it
                }

                sumPrice.observe(viewLifecycleOwner) {
                    tvTotalPrice.text = currencyIndonesia(it.toString())
                }

                cbSelectAll.setOnClickListener {
                    updateSelectedAllProduct(cbSelectAll.isChecked)
                }
            }
        }
    }
}