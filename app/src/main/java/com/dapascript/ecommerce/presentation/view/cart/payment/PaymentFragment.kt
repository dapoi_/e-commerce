package com.dapascript.ecommerce.presentation.view.cart.payment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.data.source.remote.model.PaymentData
import com.dapascript.ecommerce.databinding.FragmentPaymentBinding
import com.dapascript.ecommerce.presentation.adapter.payment.PaymentAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.cart.checkout.CheckoutFragment
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PaymentFragment : BaseFragment<FragmentPaymentBinding>(FragmentPaymentBinding::inflate) {

    private lateinit var paymentAdapter: PaymentAdapter
    @Inject
    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
        setAdapter()
    }

    private fun setAdapter() {
        paymentAdapter = PaymentAdapter(
            onItemClick = { paymentItem ->
                // disable click item
                if (!paymentItem.status) return@PaymentAdapter

                val image = paymentItem.image
                val label = paymentItem.label
                setFragmentResult(
                    CheckoutFragment.PAYMENT_REQUEST_KEY,
                    Bundle().apply {
                        putString(CheckoutFragment.PAYMENT_IMAGE, image)
                        putString(CheckoutFragment.PAYMENT_LABEL, label)
                    }
                )
                findNavController().popBackStack()
            }
        )
        binding.rvPayment.adapter = paymentAdapter

        val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
        val paymentJson = firebaseRemoteConfig.getString("payment")
        val paymentListResult: JsonAdapter<List<PaymentData>> =
            moshi.adapter(
                Types.newParameterizedType(
                    List::class.java,
                    PaymentData::class.java
                )
            )
        val paymentList = paymentListResult.fromJson(paymentJson)
        paymentList?.let { paymentAdapter.setListPayment(it) }
    }
}