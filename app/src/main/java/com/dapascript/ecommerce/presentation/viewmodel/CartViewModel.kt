package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.data.source.local.model.ProductCheckoutEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val cartRepository: CartRepository
) : ViewModel() {

    val isEachProductSelected: LiveData<Boolean> = cartRepository.getCartProduct().map { product ->
        product.isNotEmpty() && product.all { productCartEntity ->
            productCartEntity.isSelected
        }
    }.asLiveData().distinctUntilChanged()

    val isAnyProductSelected: LiveData<Boolean> =
        cartRepository.getSelectedProduct().map { product ->
            product.isNotEmpty()
        }.asLiveData().distinctUntilChanged()

    val sumPrice: LiveData<Int> = cartRepository.getCartProduct().map { allProduct ->
        allProduct.filter { eachProduct ->
            eachProduct.isSelected
        }.sumOf { product ->
            product.productPrice.toInt() * product.productQuantity
        }
    }.asLiveData().distinctUntilChanged()

    val getSelectedProduct: LiveData<List<ProductCheckoutEntity>> =
        cartRepository.getSelectedProduct().map {
            it.map { productCartEntity ->
                ProductCheckoutEntity(
                    productCartEntity.id,
                    productCartEntity.productImage,
                    productCartEntity.productName,
                    productCartEntity.productVariant,
                    productCartEntity.productStock,
                    productCartEntity.productPrice,
                    productCartEntity.productQuantity,
                    productCartEntity.isSelected
                )
            }
        }.asLiveData()

    fun getCartProduct(): LiveData<List<ProductCartEntity>> =
        cartRepository.getCartProduct().asLiveData()

    fun deleteProduct(id: String) {
        viewModelScope.launch {
            cartRepository.deleteProduct(id)
        }
    }

    fun deleteAllProduct() {
        viewModelScope.launch {
            cartRepository.deleteAllProduct()
        }
    }

    fun updateSelectedAllProduct(isSelected: Boolean) {
        viewModelScope.launch {
            cartRepository.updateAllSelectedProduct(isSelected)
        }
    }

    fun updateCartProduct(productCartEntity: ProductCartEntity) {
        viewModelScope.launch {
            cartRepository.updateProduct(productCartEntity)
        }
    }
}