package com.dapascript.ecommerce.presentation.view.auth

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.AuthDirections
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentSignInBinding
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.AuthViewModel
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.dapascript.ecommerce.utils.changedSpecificTermsConditionColor
import com.dapascript.ecommerce.utils.hideKeyboard
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SignInFragment : BaseFragment<FragmentSignInBinding>(
    FragmentSignInBinding::inflate
) {

    private val dataStoreViewModel: DataStoreViewModel by activityViewModels()
    private val authViewModel: AuthViewModel by viewModels()
    private var emailValid = false
    private var passwordValid = false

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            changedSpecificTermsConditionColor(tvTermsCondition, requireContext())

            authViewModel.apply {
                checkInputUser(this)

                btnSignIn.setOnClickListener {
                    hideKeyboard(requireActivity())
                    signInAccount(
                        etSignInEmail.text.toString(),
                        etSignInPassword.text.toString()
                    )

                    analyticsManager.buttonClickEvent(btnSignIn.text.toString())
                }

                btnSignUp.setOnClickListener {
                    hideKeyboard(requireActivity())
                    findNavController().navigate(SignInFragmentDirections.actionSignInFragmentToSignUpFragment())
                }
            }
        }

        dataStoreViewModel.getOnboardingState.observe(viewLifecycleOwner) { onBoardingHasSkipped ->
            if (!onBoardingHasSkipped) {
                findNavController().navigate(SignInFragmentDirections.actionSignInFragmentToOnboardingFragment())
            }
        }
    }

    private fun checkInputUser(authViewModel: AuthViewModel) {
        binding.apply {
            etSignInEmail.doOnTextChanged { text, _, _, _ ->
                val emailInput = text.toString()
                val emailMatches = Patterns.EMAIL_ADDRESS.matcher(text.toString()).matches()
                if (emailInput == "" || emailMatches) {
                    emailValid = true
                    tilSignInEmail.isErrorEnabled = false
                    tvEmailDesc.visibility = View.VISIBLE
                } else {
                    emailValid = false
                    tilSignInEmail.error = "Email tidak valid"
                    tvEmailDesc.visibility = View.GONE
                }

                updateButtonState()
            }

            etSignInPassword.doOnTextChanged { text, _, _, _ ->
                val passwordInput = text.toString()
                val passwordMatches = passwordInput.length >= 8
                if (passwordInput == "" || passwordMatches) {
                    passwordValid = true
                    tilSignInPassword.isErrorEnabled = false
                    tvPasswordDesc.visibility = View.VISIBLE
                } else {
                    passwordValid = false
                    tilSignInPassword.error = "Kata sandi minimal 8 karakter"
                    tvPasswordDesc.visibility = View.GONE
                }

                updateButtonState()
            }

            authViewModel.loginState.observe(viewLifecycleOwner) { result ->
                progressBar.isVisible = result is Resource.Loading

                if (result is Resource.Success) {
                    val response = result.data.data
                    dataStoreViewModel.apply {
                        saveUsername(response.userName)
                        saveAccessToken(response.accessToken)
                        saveRefreshToken(response.refreshToken)
                    }
                    findNavController().navigate(
                        AuthDirections.actionGlobalMain()
                    )
                } else if (result is Resource.Error) {
                    Toast.makeText(
                        requireContext(),
                        result.throwable.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun updateButtonState() {
        binding.btnSignIn.isEnabled = emailValid && passwordValid
    }
}