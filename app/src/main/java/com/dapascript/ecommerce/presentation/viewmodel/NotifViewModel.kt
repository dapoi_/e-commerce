package com.dapascript.ecommerce.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.dapascript.ecommerce.data.repository.notif.NotifRepository
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotifViewModel @Inject constructor(
    private val notifRepository: NotifRepository
) : ViewModel() {

    val getAllNotif: LiveData<List<NotifEntity>> = notifRepository.getAllNotif().asLiveData()

    val setReadNotif: (NotifEntity) -> Unit = { notifEntity ->
        viewModelScope.launch {
            notifRepository.readNotif(notifEntity, true)
        }
    }

    fun deleteAllNotif() {
        viewModelScope.launch {
            notifRepository.deleteAllNotif()
        }
    }
}