package com.dapascript.ecommerce.presentation.adapter.product


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.ProductCartEntity
import com.dapascript.ecommerce.databinding.ItemListCartBinding
import com.dapascript.ecommerce.utils.currencyIndonesia

class CartAdapter(
    private var onCheckEachItem: ((ProductCartEntity) -> Unit)? = null,
    private var onDeleteItem: ((ProductCartEntity) -> Unit)? = null,
    private var onIncreaseQuantity: ((ProductCartEntity) -> Unit)? = null,
    private var onDecreaseQuantity: ((ProductCartEntity) -> Unit)? = null
) : ListAdapter<ProductCartEntity, CartAdapter.CartViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        return CartViewHolder(
            ItemListCartBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val product = getItem(position)
        holder.bind(product)
    }

    inner class CartViewHolder(
        val binding: ItemListCartBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(product: ProductCartEntity) {
            binding.apply {
                Glide.with(itemView.context).load(product.productImage).into(ivProduct)
                tvProductName.text = product.productName
                tvProductVariant.text = product.productVariant

                val totalPrice = product.productPrice.toInt() * product.productQuantity
                tvProductPrice.text = currencyIndonesia(totalPrice.toString())

                val productIsRunningOut = isProductRunningOut(product)
                if (productIsRunningOut) tvProductStock.setTextColor(itemView.context.getColor(R.color.red))
                tvProductStock.text = itemView.context.getString(
                    if (productIsRunningOut) R.string.stock_remaining else R.string.stock_product,
                    product.productStock
                )

                cbProduct.isChecked = product.isSelected

                toggleButton.addOnButtonCheckedListener { group, _, _ ->
                    group.clearChecked()
                }

                cbProduct.setOnClickListener {
                    onCheckEachItem?.invoke(product)
                }

                btnMinus.setOnClickListener {
                    if (product.productQuantity > 1) {
                        onDecreaseQuantity?.invoke(product)
                    }
                }

                btnPlus.setOnClickListener {
                    if (product.productQuantity < product.productStock.toInt()) {
                        onIncreaseQuantity?.invoke(product)
                    }
                }

                btnQuantity.text = product.productQuantity.toString()

                ivDelete.setOnClickListener {
                    onDeleteItem?.invoke(product)
                }
            }
        }
    }

    private fun isProductRunningOut(product: ProductCartEntity): Boolean =
        product.productStock.toInt() < 10

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ProductCartEntity>() {
            override fun areItemsTheSame(
                oldItem: ProductCartEntity, newItem: ProductCartEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ProductCartEntity, newItem: ProductCartEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}
