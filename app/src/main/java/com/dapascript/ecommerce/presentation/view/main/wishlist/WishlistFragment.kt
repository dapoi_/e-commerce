package com.dapascript.ecommerce.presentation.view.main.wishlist

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.databinding.FragmentWishlistBinding
import com.dapascript.ecommerce.presentation.adapter.product.WishlistAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.MainActivity
import com.dapascript.ecommerce.presentation.viewmodel.WishlistViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WishlistFragment : BaseFragment<FragmentWishlistBinding>(
    FragmentWishlistBinding::inflate
) {

    private val wishlistViewModel: WishlistViewModel by viewModels()

    private lateinit var wishlistAdapter: WishlistAdapter
    private lateinit var wishListGridManager: GridLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            ivChangeView.setOnClickListener {
                wishlistViewModel.isLinearLayout()
            }

            rvWishlist.apply {
                wishlistAdapter = WishlistAdapter(
                    deleteItem = { product ->
                        wishlistViewModel.deleteWishlistId(product.id)
                    },
                    onClickItem = { product ->
                        val id = product.id
                        findNavController().navigate(
                            WishlistFragmentDirections.actionWishlistFragmentToDetailProductFragment(
                                id
                            )
                        )
                    },
                    addToCart = { product ->
                        wishlistViewModel.insertToCart(product)

                        (activity as MainActivity).customSnackbar(
                            getString(R.string.add_to_cart),
                            false
                        )
                    }
                )
                adapter = wishlistAdapter
                wishListGridManager = GridLayoutManager(requireContext(), 2)
                layoutManager = wishListGridManager

                val isLinearLayoutObserver = { isLinearLayout: Boolean ->
                    wishlistAdapter.setViewType(if (isLinearLayout) 0 else 1)
                    binding.ivChangeView.setImageResource(if (isLinearLayout) R.drawable.ic_list else R.drawable.ic_grid)
                    wishListGridManager.spanCount = if (isLinearLayout) 1 else 2
                }

                wishlistViewModel.isLinearLayout.observe(viewLifecycleOwner, isLinearLayoutObserver)
            }

            wishlistViewModel.wishlist.observe(viewLifecycleOwner) { response ->
                binding.apply {
                    wishlistAdapter.submitList(response)

                    layoutError.btnError.visibility = View.GONE
                    layoutError.root.isVisible = response.isNullOrEmpty()
                    rvWishlist.isVisible = !response.isNullOrEmpty()
                    tvTotalWishlist.isVisible = !response.isNullOrEmpty()
                    div.isVisible = !response.isNullOrEmpty()
                    ivChangeView.isVisible = !response.isNullOrEmpty()
                    tvTotalWishlist.text = getString(
                        R.string.total_wishlist,
                        wishlistAdapter.currentList.size.toString()
                    )
                }
            }
        }
    }
}