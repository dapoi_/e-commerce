package com.dapascript.ecommerce.presentation.adapter.product

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.ProductCheckoutEntity
import com.dapascript.ecommerce.databinding.ItemListCheckoutBinding
import com.dapascript.ecommerce.utils.currencyIndonesia

class ProductCheckoutAdapter(
    private val totalPriceChangedListener: ((Int) -> Unit)? = null
) : ListAdapter<ProductCheckoutEntity, ProductCheckoutAdapter.CheckoutViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckoutViewHolder {
        return CheckoutViewHolder(
            ItemListCheckoutBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: CheckoutViewHolder, position: Int) {
        val product = getItem(position)
        holder.bind(product)
    }

    inner class CheckoutViewHolder(
        private val binding: ItemListCheckoutBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.btnMinus.setOnClickListener {
                val product = getItem(absoluteAdapterPosition)
                if (product.productQuantity > 1) {
                    product.productQuantity -= 1
                    updateItemTotalPrice(product, absoluteAdapterPosition)
                }
            }

            binding.btnPlus.setOnClickListener {
                val product = getItem(absoluteAdapterPosition)
                if (product.productQuantity < product.productStock.toInt()) {
                    product.productQuantity += 1
                    updateItemTotalPrice(product, absoluteAdapterPosition)
                }
            }
        }

        @SuppressLint("SetTextI18n")
        fun bind(product: ProductCheckoutEntity) {
            binding.apply {
                Glide.with(itemView.context).load(product.productImage).into(ivProduct)
                tvProductName.text = product.productName
                tvProductVariant.text = product.productVariant

                val totalPrice = product.productPrice.toInt() * product.productQuantity
                tvProductPrice.text = currencyIndonesia(totalPrice.toString())

                val productIsRunningOut = isProductRunningOut(product)
                if (productIsRunningOut) tvProductStock.setTextColor(itemView.context.getColor(R.color.red))
                tvProductStock.text = itemView.context.getString(
                    if (productIsRunningOut) R.string.stock_remaining else R.string.stock_product,
                    product.productStock
                )

                btnQuantity.text = product.productQuantity.toString()
                toggleButton.addOnButtonCheckedListener { group, _, _ ->
                    group.clearChecked()
                }
            }
        }
    }

    private fun calculateTotalPrice(): Int {
        var totalPrice = 0
        for (i in 0 until itemCount) {
            val product = getItem(i)
            totalPrice += product.productPrice.toInt() * product.productQuantity
        }
        return totalPrice
    }

    private fun updateItemTotalPrice(product: ProductCheckoutEntity, position: Int) {
        val totalPrice = product.productPrice.toInt() * product.productQuantity
        notifyItemChanged(position, totalPrice)
        totalPriceChangedListener?.invoke(calculateTotalPrice())
    }

    private fun isProductRunningOut(product: ProductCheckoutEntity): Boolean =
        product.productStock.toInt() < 10

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ProductCheckoutEntity>() {
            override fun areItemsTheSame(
                oldItem: ProductCheckoutEntity,
                newItem: ProductCheckoutEntity
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ProductCheckoutEntity,
                newItem: ProductCheckoutEntity
            ): Boolean {
                return oldItem == newItem
            }
        }
    }
}