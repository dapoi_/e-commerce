package com.dapascript.ecommerce.presentation.view.detail.review

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.dapascript.ecommerce.data.source.remote.model.ReviewsItem
import com.dapascript.ecommerce.databinding.FragmentUserReviewBinding
import com.dapascript.ecommerce.presentation.adapter.review.ReviewAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.ReviewViewModel
import com.dapascript.ecommerce.utils.handleError
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserReviewFragment : BaseFragment<FragmentUserReviewBinding>(
    FragmentUserReviewBinding::inflate
) {

    private val reviewViewModel: ReviewViewModel by viewModels()
    private val args: UserReviewFragmentArgs by navArgs()
    private val reviewAdapter: ReviewAdapter by lazy { ReviewAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
        setViewModel()
    }

    private fun setViewModel() {
        reviewViewModel.getReviews(args.productId)
        reviewViewModel.getReviewsResult.observe(viewLifecycleOwner) { response ->
            binding.apply {
                progressBar.isVisible = response is Resource.Loading
                rvUserReview.isVisible = response is Resource.Success
                layoutError.root.isVisible = response is Resource.Error

                if (response is Resource.Success) {
                    initUI(response.data)
                } else if (response is Resource.Error) {
                    handleError(
                        layoutError,
                        response.throwable,
                        requireContext()
                    )
                    layoutError.btnError.setOnClickListener { setViewModel() }
                }
            }
        }
    }

    private fun initUI(response: List<ReviewsItem>) {
        binding.rvUserReview.apply {
            adapter = reviewAdapter
            layoutManager = LinearLayoutManager(requireContext())
            reviewAdapter.setListReview(response)
        }
    }
}