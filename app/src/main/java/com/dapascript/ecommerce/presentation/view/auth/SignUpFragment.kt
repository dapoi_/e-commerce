package com.dapascript.ecommerce.presentation.view.auth

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.dapascript.ecommerce.AuthDirections
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.databinding.FragmentSignUpBinding
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.viewmodel.AuthViewModel
import com.dapascript.ecommerce.presentation.viewmodel.DataStoreViewModel
import com.dapascript.ecommerce.utils.changedSpecificTermsConditionColor
import com.dapascript.ecommerce.utils.hideKeyboard
import com.dapascript.ecommerce.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SignUpFragment : BaseFragment<FragmentSignUpBinding>(
    FragmentSignUpBinding::inflate
) {

    private val authViewModel: AuthViewModel by viewModels()
    private val dataStoreViewModel: DataStoreViewModel by viewModels()
    private var emailValid = false
    private var passwordValid = false
    @Inject
    lateinit var analyticsManager: AnalyticsManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            changedSpecificTermsConditionColor(tvTermsCondition, requireActivity())

            authViewModel.apply {
                checkInputUser(this)

                btnSignUp.setOnClickListener {
                    hideKeyboard(requireActivity())
                    signUpAccount(
                        etSignUpEmail.text.toString(),
                        etSignUpPassword.text.toString()
                    )

                    analyticsManager.buttonClickEvent(btnSignUp.text.toString())
                }

                btnSignIn.setOnClickListener {
                    hideKeyboard(requireActivity())
                    findNavController().navigate(SignUpFragmentDirections.actionSignUpFragmentToSignInFragment())
                }
            }
        }
    }

    private fun checkInputUser(authViewModel: AuthViewModel) {
        binding.apply {
            etSignUpEmail.doOnTextChanged { text, _, _, _ ->
                val emailInput = text.toString()
                val emailMatches = Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()
                if (emailInput == "" || emailMatches) {
                    emailValid = true
                    tilSignUpEmail.isErrorEnabled = false
                    tvEmailDesc.visibility = View.VISIBLE
                } else {
                    emailValid = false
                    tilSignUpEmail.error = "Email tidak valid"
                    tvEmailDesc.visibility = View.GONE
                }

                updateButtonState()
            }

            etSignUpPassword.doOnTextChanged { text, _, _, _ ->
                val passwordInput = text.toString()
                val passwordMatches = passwordInput.length >= 8
                if (passwordInput == "" || passwordMatches) {
                    passwordValid = true
                    tilSignUpPassword.isErrorEnabled = false
                    tvPasswordDesc.visibility = View.VISIBLE
                } else {
                    passwordValid = false
                    tilSignUpPassword.error = "Kata sandi minimal 8 karakter"
                    tvPasswordDesc.visibility = View.GONE
                }

                updateButtonState()
            }

            authViewModel.registerState.observe(viewLifecycleOwner) { result ->
                progressBar.isVisible = result is Resource.Loading

                if (result is Resource.Success) {
                    val response = result.data.data
                    dataStoreViewModel.apply {
                        saveAccessToken(response.accessToken)
                        saveRefreshToken(response.refreshToken)
                    }
                    findNavController().navigate(
                        AuthDirections.actionGlobalMain()
                    )
                } else if (result is Resource.Error) {
                    Toast.makeText(
                        requireContext(),
                        result.throwable.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun updateButtonState() {
        binding.btnSignUp.isEnabled = emailValid && passwordValid
    }
}