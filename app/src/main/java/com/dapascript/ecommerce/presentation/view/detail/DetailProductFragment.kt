package com.dapascript.ecommerce.presentation.view.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.analytics.AnalyticsManager
import com.dapascript.ecommerce.data.source.local.model.ProductCheckoutEntity
import com.dapascript.ecommerce.data.source.remote.model.DetailItem
import com.dapascript.ecommerce.databinding.FragmentDetailProductBinding
import com.dapascript.ecommerce.presentation.adapter.product.ProductImageAdapter
import com.dapascript.ecommerce.presentation.view.BaseFragment
import com.dapascript.ecommerce.presentation.view.MainActivity
import com.dapascript.ecommerce.presentation.viewmodel.CartViewModel
import com.dapascript.ecommerce.presentation.viewmodel.DetailViewModel
import com.dapascript.ecommerce.presentation.viewmodel.NotifViewModel
import com.dapascript.ecommerce.utils.currencyIndonesia
import com.dapascript.ecommerce.utils.handleError
import com.dapascript.ecommerce.vo.Resource
import com.google.android.material.chip.Chip
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailProductFragment : BaseFragment<FragmentDetailProductBinding>(
    FragmentDetailProductBinding::inflate
) {

    private val detailViewModel: DetailViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val notifViewModel: NotifViewModel by viewModels()
    private val args: DetailProductFragmentArgs by navArgs()
    private val productImageAdapter: ProductImageAdapter by lazy { ProductImageAdapter() }
    private var chipVariant: String = ""
    private var currentPrice: Int = 0

    @Inject
    lateinit var analyticsManager: AnalyticsManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        binding.apply {
            toolbar.setNavigationOnClickListener { findNavController().popBackStack() }
            ivCart.setOnClickListener {
                findNavController().navigate(DetailProductFragmentDirections.actionDetailProductFragmentToCartFragment())
            }
            ivNotif.setOnClickListener {
                findNavController().navigate(DetailProductFragmentDirections.actionDetailProductFragmentToNotifFragment())
            }
        }
        setViewModel()
    }

    private fun setViewModel() {
        detailViewModel.getDetail(args.productId)
        detailViewModel.getDetailResult.observe(viewLifecycleOwner) { response ->
            binding.apply {
                progressBar.isVisible = response is Resource.Loading
                svProductDetail.isVisible = response is Resource.Success
                layoutError.root.isVisible = response is Resource.Error
                btnAddCart.isVisible = response !is Resource.Error
                btnBuyNow.isVisible = response !is Resource.Error

                if (response is Resource.Success) {
                    initUI(response.data)
                } else if (response is Resource.Error) {
                    handleError(
                        layoutError,
                        response.throwable,
                        requireContext()
                    )
                    layoutError.btnError.setOnClickListener { setViewModel() }
                }
            }
        }

        detailViewModel.checkProductWishlist(args.productId)
            .observe(viewLifecycleOwner) { isWishlist ->
                binding.apply {
                    if (isWishlist) {
                        ivProductFav.setImageResource(R.drawable.ic_fav_true)
                    } else {
                        ivProductFav.setImageResource(R.drawable.ic_fav_false)
                    }

                    val message = if (isWishlist) {
                        getString(R.string.remove_wishlist)
                    } else {
                        getString(R.string.add_wishlist)
                    }
                    ivProductFav.setOnClickListener {
                        (activity as MainActivity).customSnackbar(message, isWishlist)
                        if (isWishlist) {
                            detailViewModel.deleteFromWishlist()
                        } else {
                            detailViewModel.insertToWishlist()
                        }
                    }
                }
            }

        cartViewModel.getCartProduct().observe(viewLifecycleOwner) { cartProduct ->
            binding.apply {
                if (cartProduct.isNotEmpty()) {
                    tvCartBadge.visibility = View.VISIBLE
                    tvCartBadge.text = cartProduct.size.toString()
                } else {
                    tvCartBadge.visibility = View.GONE
                }
            }
        }

        notifViewModel.getAllNotif.observe(viewLifecycleOwner) { response ->
            response.filter { !it.isRead }.let { unreadNotif ->
                binding.apply {
                    if (unreadNotif.isEmpty()) {
                        tvNotifBadge.visibility = View.GONE
                    } else {
                        tvNotifBadge.visibility = View.VISIBLE
                        tvNotifBadge.text = unreadNotif.size.toString()
                    }
                }
            }
        }
    }

    @SuppressLint("StringFormatMatches")
    private fun initUI(response: DetailItem) {
        binding.apply {
            productImageAdapter.setImage(response.image)
            vpProductImage.adapter = productImageAdapter
            vpProductImage.offscreenPageLimit = 1

            tabIndicator.isVisible = productImageAdapter.itemCount > 1
            TabLayoutMediator(tabIndicator, vpProductImage) { tab, _ ->
                vpProductImage.setCurrentItem(tab.position, true)
            }.attach()

            tvProductPrice.text = currencyIndonesia(response.productPrice.toString())
            tvProductName.text = response.productName
            tvTotalSold.text = getString(R.string.total_sold, response.sale.toString())
            chipProductRating.isClickable = false
            chipProductRating.text = getString(
                R.string.product_rating,
                response.productRating.toString(),
                response.totalRating
            )

            cgVariant.removeAllViews()
            response.productVariant.forEachIndexed { index, variantItem ->
                val chip = Chip(requireContext())
                chip.text = variantItem.variantName
                chip.isCheckable = true

                if (index == 0) {
                    chip.isChecked = true
                    chipVariant = variantItem.variantName
                    currentPrice = variantItem.variantPrice + response.productPrice
                    tvProductPrice.text = currencyIndonesia(currentPrice.toString())
                }

                chip.setOnClickListener {
                    chipVariant = variantItem.variantName
                    currentPrice = variantItem.variantPrice + response.productPrice
                    tvProductPrice.text = currencyIndonesia(currentPrice.toString())
                }

                cgVariant.addView(chip)
            }


            tvProductDescription.text = response.description
            tvUserReview.text = response.productRating.toString()
            tvSatisfaction.text = getString(
                R.string.buyer_happy, response.totalSatisfaction,
            )
            tvTotalRating.text = getString(
                R.string.total_rating_review, response.totalRating, response.totalReview
            )

            btnSeeAllReview.setOnClickListener {
                val id = args.productId
                findNavController().navigate(
                    DetailProductFragmentDirections.actionDetailProductFragmentToUserReviewFragment(
                        id
                    )
                )
            }

            btnAddCart.setOnClickListener {
                detailViewModel.insertToCart(response)

                (activity as MainActivity).customSnackbar(
                    getString(R.string.add_to_cart),
                    false
                )

                analyticsManager.beginCheckoutEvent(response)
                analyticsManager.buttonClickEvent(btnAddCart.text.toString())
            }

            btnBuyNow.setOnClickListener {
                val productEntity = ProductCheckoutEntity(
                    id = response.productId,
                    productImage = response.image[0],
                    productName = response.productName,
                    productVariant = chipVariant,
                    productStock = response.stock.toString(),
                    productPrice = currentPrice.toString()
                )
                findNavController().navigate(
                    DetailProductFragmentDirections.actionDetailProductFragmentToCheckoutFragment(
                        checkoutEntity = arrayOf(productEntity)
                    )
                )

                analyticsManager.beginCheckoutEvent(response)
                analyticsManager.buttonClickEvent(btnBuyNow.text.toString())
            }
        }

        analyticsManager.viewItemEvent(response)
    }
}