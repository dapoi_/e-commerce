package com.dapascript.ecommerce.di

import android.content.Context
import androidx.room.Room
import com.dapascript.ecommerce.data.source.local.db.ProductDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun getProductDB(@ApplicationContext context: Context): ProductDB = Room.databaseBuilder(
        context, ProductDB::class.java, "product.db"
    ).fallbackToDestructiveMigration().build()

    @Provides
    fun getCartDao(db: ProductDB) = db.cartDao()

    @Provides
    fun getWishlistDao(db: ProductDB) = db.wishlistDao()

    @Provides
    fun getNotifDao(db: ProductDB) = db.notifDao()
}