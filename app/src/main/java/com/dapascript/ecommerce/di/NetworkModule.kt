package com.dapascript.ecommerce.di

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.dapascript.ecommerce.data.preference.DataStorePreference
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.utils.auth.AuthInterceptor
import com.dapascript.ecommerce.utils.auth.TokenAuthenticator
import com.dapascript.ecommerce.utils.ipConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideOkhttpClient(
        authInterceptor: AuthInterceptor,
        tokenAuthenticator: TokenAuthenticator,
        @ApplicationContext context: Context
    ): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val chuckerInterceptor =
            ChuckerInterceptor.Builder(context).collector(ChuckerCollector(context))
                .maxContentLength(250000L).alwaysReadResponseBody(true).build()

        return OkHttpClient.Builder()
            .addInterceptor(authInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(chuckerInterceptor)
            .authenticator(tokenAuthenticator)
            .build()
    }

    @Singleton
    @Provides
    fun provideAuthInterceptor(dataStorePreference: DataStorePreference): AuthInterceptor =
        AuthInterceptor(dataStorePreference)

    @Singleton
    @Provides
    fun provideAuthAuthenticator(
        dataStorePreference: DataStorePreference,
        @ApplicationContext context: Context,
    ): TokenAuthenticator = TokenAuthenticator(dataStorePreference, context)

    @Singleton
    @Provides
    fun provideRetrofitBuilder(): Retrofit.Builder {
        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        return Retrofit.Builder()
            .baseUrl(ipConfig[1])
            .addConverterFactory(MoshiConverterFactory.create(moshi))
    }

    @Singleton
    @Provides
    fun provideApiService(
        retrofit: Retrofit.Builder,
        okHttpClient: OkHttpClient,
    ): ApiService {
        return retrofit.client(okHttpClient)
            .build()
            .create(ApiService::class.java)
    }

    @IoDispatcher
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO
}