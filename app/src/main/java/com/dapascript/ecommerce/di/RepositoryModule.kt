package com.dapascript.ecommerce.di

import com.dapascript.ecommerce.data.repository.auth.AuthRepository
import com.dapascript.ecommerce.data.repository.auth.AuthRepositoryImpl
import com.dapascript.ecommerce.data.repository.cart.CartRepository
import com.dapascript.ecommerce.data.repository.cart.CartRepositoryImpl
import com.dapascript.ecommerce.data.repository.cart.fulfillment.FulfillmentRepository
import com.dapascript.ecommerce.data.repository.cart.fulfillment.FulfillmentRepositoryImpl
import com.dapascript.ecommerce.data.repository.detail.DetailRepository
import com.dapascript.ecommerce.data.repository.detail.DetailRepositoryImpl
import com.dapascript.ecommerce.data.repository.detail.review.ReviewRepository
import com.dapascript.ecommerce.data.repository.detail.review.ReviewRepositoryImpl
import com.dapascript.ecommerce.data.repository.main.store.StoreRepository
import com.dapascript.ecommerce.data.repository.main.store.StoreRepositoryImpl
import com.dapascript.ecommerce.data.repository.main.store.search.SearchRepository
import com.dapascript.ecommerce.data.repository.main.store.search.SearchRepositoryImpl
import com.dapascript.ecommerce.data.repository.main.transaction.TransactionRepository
import com.dapascript.ecommerce.data.repository.main.transaction.TransactionRepositoryImpl
import com.dapascript.ecommerce.data.repository.main.wishlist.WishlistRepository
import com.dapascript.ecommerce.data.repository.main.wishlist.WishlistRepositoryImpl
import com.dapascript.ecommerce.data.repository.notif.NotifRepository
import com.dapascript.ecommerce.data.repository.notif.NotifRepositoryImpl
import com.dapascript.ecommerce.data.repository.profile.ProfileRepository
import com.dapascript.ecommerce.data.repository.profile.ProfileRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module(includes = [NetworkModule::class, DatabaseModule::class])
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideAuthRepository(
        authRepositoryImpl: AuthRepositoryImpl
    ): AuthRepository

    @Binds
    abstract fun provideMainRepository(
        mainRepositoryImpl: ProfileRepositoryImpl
    ): ProfileRepository

    @Binds
    abstract fun provideStoreRepository(
        storeRepositoryImpl: StoreRepositoryImpl
    ): StoreRepository

    @Binds
    abstract fun provideSearchRepository(
        searchRepositoryImpl: SearchRepositoryImpl
    ): SearchRepository

    @Binds
    abstract fun provideDetailRepository(
        detailRepositoryImpl: DetailRepositoryImpl
    ): DetailRepository

    @Binds
    abstract fun provideReviewRepository(
        reviewRepositoryImpl: ReviewRepositoryImpl
    ): ReviewRepository

    @Binds
    abstract fun provideCartRepository(
        cartRepositoryImpl: CartRepositoryImpl
    ): CartRepository

    @Binds
    abstract fun provideWishlistRepository(
        wishlistRepositoryImpl: WishlistRepositoryImpl
    ): WishlistRepository

    @Binds
    abstract fun provideFulfillmentRepository(
        fulfillmentRepositoryImpl: FulfillmentRepositoryImpl
    ): FulfillmentRepository

    @Binds
    abstract fun provideTransactionRepository(
        transactionRepositoryImpl: TransactionRepositoryImpl
    ): TransactionRepository

    @Binds
    abstract fun provideNotifRepository(
        notifRepositoryImpl: NotifRepositoryImpl
    ): NotifRepository
}