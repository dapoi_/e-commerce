package com.dapascript.ecommerce.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.db.NotifDao
import com.dapascript.ecommerce.data.source.local.model.NotifEntity
import com.dapascript.ecommerce.presentation.view.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import java.util.Random
import javax.inject.Inject

@AndroidEntryPoint
class AppNotification : FirebaseMessagingService() {

    @Inject
    lateinit var notifDao: NotifDao

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d("TOKEN", token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val title = message.notification?.title
        val desc = message.notification?.body
        val image = message.notification?.imageUrl
        val date = message.data["date"]
        val time = message.data["time"]
        val type = message.data["type"]
        val notifEntity = NotifEntity(
            title = title,
            desc = desc,
            image = image.toString(),
            type = type,
            date = date,
            time = time,
        )

        notifDao.insertNotif(notifEntity)
        sendNotification(title, desc)
    }

    private fun sendNotification(title: String?, desc: String?) {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val randomId = Random().nextInt(100)
        val notificationIntent = Intent(this, MainActivity::class.java).apply {
            action = Intent.ACTION_MAIN
            addCategory(Intent.CATEGORY_LAUNCHER)
            data = Uri.parse("ecommerce://notif/$randomId")
        }

        val pendingIntent = notificationIntent.let {
            PendingIntent.getActivity(
                this,
                randomId,
                it,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(desc)
            .setSmallIcon(R.drawable.ic_ecommerce_plain)
            .setStyle(NotificationCompat.BigTextStyle().bigText(desc))
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID, "payment_channel", NotificationManager.IMPORTANCE_HIGH
            ).apply {
                enableVibration(true)
                vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)
            }
            notification.setChannelId(CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(randomId, notification.build())
    }

    companion object {
        private const val CHANNEL_ID = "channel_id"
    }
}