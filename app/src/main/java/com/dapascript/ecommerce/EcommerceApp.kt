package com.dapascript.ecommerce

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class EcommerceApp : Application()