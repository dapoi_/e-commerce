package com.dapascript.ecommerce.utils

import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.data.source.local.model.ProductEntity
import kotlin.math.roundToInt

object ProductDummy {

    private val productId = listOf(
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8"
    )

    private val imageProduct = listOf(
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
        R.drawable.ic_image_default,
    )

    private val nameProduct = listOf(
        "Produk 1",
        "Produk 2",
        "Produk 3",
        "Produk 4",
        "Produk 5",
        "Produk 6",
        "Produk 7",
        "Produk 8",
    )

    private val priceProduct = listOf(
        100000.0,
        200000.0,
        300000.0,
        400000.0,
        500000.0,
        600000.0,
        700000.0,
        800000.0,
    )

    private val sellerNameProduct = listOf(
        "Penjual 1",
        "Penjual 2",
        "Penjual 3",
        "Penjual 4",
        "Penjual 5",
        "Penjual 6",
        "Penjual 7",
        "Penjual 8",
    )

    private val rateProduct = listOf(
        "4.5",
        "5.0",
        "4.0",
        "4.5",
        "5.0",
        "4.0",
        "4.5",
        "4.5"
    )

    private val totalSold = listOf(
        "100 Terjual",
        "200 Terjual",
        "300 Terjual",
        "400 Terjual",
        "500 Terjual",
        "600 Terjual",
        "700 Terjual",
        "800 Terjual",
    )

    private fun getProduct(): List<ProductEntity> {
        val listProduct = ArrayList<ProductEntity>()
        for (position in imageProduct.indices) {
            val product = ProductEntity(
                productId = productId[position],
                image = imageProduct[position].toString(),
                name = nameProduct[position],
                price = priceProduct[position].roundToInt(),
                sellerName = sellerNameProduct[position],
                rating = rateProduct[position],
                sold = totalSold[position],
                brand = "Brand"
            )
            listProduct.add(product)
        }
        return listProduct
    }


    fun filterProducts(
        sortOption: String?,
        categoryOption: String?,
        minPrice: Double?,
        maxPrice: Double?
    ): List<ProductEntity> {
        // Apply filters based on the selected options
        val filteredList = getProduct().toMutableList()

        sortOption?.let { sort ->
            filteredList.sortWith(compareBy {
                when (sort) {
                    "review" -> it.rating.toDouble()
                    "sales" -> it.sold.split(" ")[0].toInt()
                    "high_price" -> -it.price
                    "low_price" -> it.price
                    else -> 0.0
                }
            })
        }

        categoryOption?.let { category ->
            filteredList.retainAll { product ->
                when (category) {
                    "asus" -> product.name.contains("Asus", ignoreCase = true)
                    "lenovo" -> product.name.contains("Lenovo", ignoreCase = true)
                    else -> true
                }
            }
        }

        minPrice?.let { min ->
            filteredList.retainAll { product ->
                product.price >= min
            }
        }

        maxPrice?.let { max ->
            filteredList.retainAll { product ->
                product.price <= max
            }
        }

        return filteredList
    }
}