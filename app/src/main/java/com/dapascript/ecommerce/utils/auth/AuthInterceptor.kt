package com.dapascript.ecommerce.utils.auth

import com.dapascript.ecommerce.BuildConfig
import com.dapascript.ecommerce.data.preference.DataStorePreference
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val dataStorePreference: DataStorePreference
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = runBlocking { dataStorePreference.getAllToken.first().first }
        val endPoint = chain.request().url.encodedPath
        return if (endPoint == "/register" || endPoint == "/login" || endPoint == "/refresh") {
            val request = chain.request().newBuilder().apply {
                addHeader("API_KEY", BuildConfig.API_KEY)
            }
            chain.proceed(request.build())
        } else {
            val request = chain.request().newBuilder().apply {
                addHeader("Authorization", "Bearer $accessToken")
            }
            chain.proceed(request.build())
        }
    }
}