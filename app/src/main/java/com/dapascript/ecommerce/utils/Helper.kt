package com.dapascript.ecommerce.utils

import android.app.Activity
import android.app.Application
import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.icu.text.NumberFormat
import android.net.Uri
import android.os.Environment
import android.text.Editable
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.dapascript.ecommerce.R
import com.dapascript.ecommerce.databinding.LayoutErrorBinding
import com.google.android.material.textfield.TextInputEditText
import retrofit2.HttpException
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Locale


fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(activity.currentFocus?.windowToken, 0)
}

fun showKeyboard(activity: Activity, editText: TextInputEditText) {
    editText.requestFocus()
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(editText, 0)
}

private const val FILENAME_FORMAT = "dd-MMM-yyyy"

val timeStamp: String = SimpleDateFormat(
    FILENAME_FORMAT,
    Locale.US
).format(System.currentTimeMillis())

fun createFile(application: Application): File {
    val mediaDir = application.externalMediaDirs.firstOrNull()?.let {
        File(it, application.resources.getString(R.string.app_name)).apply { mkdirs() }
    }

    val outputDirectory = if (
        mediaDir != null && mediaDir.exists()
    ) mediaDir else application.filesDir

    return File(outputDirectory, "$timeStamp.jpg")
}

fun rotateBitmap(bitmap: Bitmap, isBackCamera: Boolean = false): Bitmap {
    val matrix = Matrix()
    return if (isBackCamera) {
        matrix.postRotate(90f)
        Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    } else {
        matrix.postRotate(-90f)
        matrix.postScale(-1f, 1f, bitmap.width / 2f, bitmap.height / 2f)
        Bitmap.createBitmap(
            bitmap,
            0,
            0,
            bitmap.width,
            bitmap.height,
            matrix,
            true
        )
    }
}

fun uriToFile(selectedImg: Uri, context: Context): File {
    val contentResolver: ContentResolver = context.contentResolver
    val myFile = createCustomTempFile(context)

    val inputStream = contentResolver.openInputStream(selectedImg) as InputStream
    val outputStream: OutputStream = FileOutputStream(myFile)
    val buf = ByteArray(1024)
    var len: Int
    while (inputStream.read(buf).also { len = it } > 0) outputStream.write(buf, 0, len)
    outputStream.close()
    inputStream.close()

    return myFile
}

fun createCustomTempFile(context: Context): File {
    val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
    return File.createTempFile(timeStamp, ".jpg", storageDir)
}

fun reduceFileImage(file: File): File {
    val bitmap = BitmapFactory.decodeFile(file.path)

    var compressQuality = 100
    var streamLength: Int

    do {
        val bmpStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
        val bmpPicByteArray = bmpStream.toByteArray()
        streamLength = bmpPicByteArray.size
        compressQuality -= 5
    } while (streamLength > 1000000)

    bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, FileOutputStream(file))

    return file
}

fun changedSpecificTermsConditionColor(tvTC: TextView, context: Context) {
    val privacyPolicyString = context.getString(R.string.terms_condition)
    val termsString = context.getString(R.string.terms)
    val conditionsString = context.getString(R.string.conditions)

    val spannablePrivacyPolicy = SpannableStringBuilder(privacyPolicyString)

    val termsStart = privacyPolicyString.indexOf(termsString)
    val termsEnd = termsStart + termsString.length

    val conditionsStart = privacyPolicyString.indexOf(conditionsString)
    val conditionsEnd = conditionsStart + conditionsString.length

    spannablePrivacyPolicy.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, R.color.purple_500)),
        termsStart,
        termsEnd,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    spannablePrivacyPolicy.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, R.color.purple_500)),
        conditionsStart,
        conditionsEnd,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    tvTC.text = spannablePrivacyPolicy
}

fun capitalizeEachWord(str: String, delimiter: String = " ", separator: String = " "): String {
    return str.split(delimiter).joinToString(separator) {
        it.lowercase().replaceFirstChar { char -> char.titlecase() }
    }
}

fun handleError(layoutError: LayoutErrorBinding, throwable: Throwable, context: Context) {
    when (throwable) {
        is HttpException -> {
            if (throwable.code() == 404) {
                showError(
                    layoutError,
                    context.getString(R.string.empty),
                    context.getString(R.string.empty_desc),
                    context.getString(R.string.reset)
                )
            } else {
                showError(
                    layoutError,
                    throwable.code().toString(),
                    context.getString(R.string.server_desc),
                    context.getString(R.string.refresh)
                )
            }
        }

        is IOException -> {
            showError(
                layoutError,
                context.getString(R.string.connection),
                context.getString(R.string.connection_desc),
                context.getString(R.string.refresh)
            )
        }
    }
}

fun showError(
    layoutError: LayoutErrorBinding,
    title: String,
    desc: String,
    textButton: CharSequence
) {
    layoutError.apply {
        root.isVisible = true
        tvError.text = title
        tvErrorDescription.text = desc
        btnError.text = textButton
    }
}

fun currencyIndonesia(price: String?): String {
    val cleanString =
        price?.replace("[Rp,.\\s]".toRegex(), "") // Remove currency symbol, comma, and spaces
    val value =
        cleanString?.toDoubleOrNull() ?: 0.0 // Convert to double or use 0.0 if conversion fails

    val localeID = Locale("in", "ID")
    val formatRupiah = NumberFormat.getCurrencyInstance(localeID).apply {
        maximumFractionDigits = 0
    }
    return formatRupiah.format(value)
}

fun applyEditTextCurrencyIndonesia(editText: TextInputEditText) {
    editText.addTextChangedListener(object : TextWatcher {
        private var current = ""

        override fun afterTextChanged(s: Editable?) {
            if (s.toString() != current) {
                editText.removeTextChangedListener(this)

                val formatted = if (s.toString().isNotEmpty()) {
                    currencyIndonesia(s.toString())
                } else {
                    ""
                }

                current = formatted
                editText.setText(formatted)
                editText.setSelection(formatted.length)

                editText.addTextChangedListener(this)
            }
        }

        override fun beforeTextChanged(
            s: CharSequence?,
            start: Int,
            count: Int,
            after: Int,
        ) {
        }

        override fun onTextChanged(
            s: CharSequence?,
            start: Int,
            before: Int,
            count: Int,
        ) {
        }
    })
}

fun convertCurrencyToNumber(currency: String): String? {
    if (currency.isEmpty()) {
        return null
    }

    val cleanString =
        currency.replace("[Rp,.\\s]".toRegex(), "") // Remove currency symbol, comma, and spaces
    val numericValue = cleanString.toDoubleOrNull()

    return numericValue?.toBigDecimal()?.stripTrailingZeros()?.toPlainString()
}

val ipConfig = arrayOf(
    "http://192.168.1.11:8080/",
    "http://172.17.20.84:8080/"
)