package com.dapascript.ecommerce.utils.auth

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.dapascript.ecommerce.BuildConfig
import com.dapascript.ecommerce.data.preference.DataStorePreference
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenRequest
import com.dapascript.ecommerce.data.source.remote.model.RefreshTokenResponse
import com.dapascript.ecommerce.data.source.remote.network.ApiService
import com.dapascript.ecommerce.utils.ipConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    private val dataStorePreference: DataStorePreference,
    private val context: Context
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            val token = runBlocking { dataStorePreference.getAllToken.first().second }
            return runBlocking {
                try {
                    val newToken = getNewToken(token)
                    val accessToken = newToken.data.accessToken
                    val refreshToken = newToken.data.refreshToken
                    dataStorePreference.apply {
                        saveAccessToken(accessToken)
                        saveRefreshToken(refreshToken)
                    }
                    response.request.newBuilder()
                        .header("Authorization", "Bearer $refreshToken")
                        .build()
                } catch (e: Exception) {
                    dataStorePreference.signOut()
                    return@runBlocking null
                }
            }
        }
    }

    private suspend fun getNewToken(refreshToken: String?): RefreshTokenResponse {
        val interceptor = Interceptor.invoke { chain ->
            val request = chain.request()
                .newBuilder()
                .addHeader("API_KEY", BuildConfig.API_KEY)
                .build()
            chain.proceed(request)
        }

        val chuckerInterceptor =
            ChuckerInterceptor.Builder(context).collector(ChuckerCollector(context))
                .maxContentLength(250000L).alwaysReadResponseBody(true).build()

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(chuckerInterceptor)
            .build()

        val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(ipConfig[1])
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .build()

        val service = retrofit.create(ApiService::class.java)
        val refreshTokenReq = RefreshTokenRequest("$refreshToken")
        return service.refreshToken(refreshTokenReq)
    }
}